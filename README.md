# InzeniusCloud

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.0.

## Installing ##

* Install [Node.js](https://nodejs.org/en/download/) version 7.1.0 or newer (check which version with `node -v`)
* * If you are using Windows 10, ensure you are using npm 7.2 or newer (see [here](https://github.com/angular/angular-cli/issues/3102) for discussion)
* Install [npm](https://docs.npmjs.com/getting-started/installing-node) version 4.0.2 or newer (check which version with `npm -v`)
* Clone the repository using `git clone https://bitbucket.org/inzeniusdev/inzenius.cloud.git`
* Checkout your desired branch using `git checkout angular-cli`
* Run the following command in the repository folder `npm install`
* To launch a local development copy of the web app run `ng serve`. The web app will now be available at `http://localhost:4200/`
* * To launch a development copy accessible by others on the network run `ng serve --host 0.0.0.0`
* * To launch a the web app on a different port run `ng serve --port xxxx`

## Updating Angular CLI ##

If you are updating from a beta version of the CLI, follow the steps [here](https://github.com/angular/angular-cli/wiki/stories-rc-update).

Otherwise, run the following steps to update.

First, update your global package:

```
npm uninstall -g @angular/cli
npm cache clean
npm install -g @angular/cli@latest
```

Then, update your local package (from within your project directory):

```
rm -rf node_modules dist # use rmdir /S/Q node_modules dist in Windows Command Prompt; use rm -r -fo node_modules,dist in Windows PowerShell
npm install --save-dev @angular/cli@latest
npm install
```

See [here](https://github.com/angular/angular-cli#updating-angular-cli) for more information on updating.

## Project Code Styling ##

This project will adhere to strict formatting rules.

These rules can be checked by running `ng lint` in your project directory. This command should be executed **before** commiting any changes.

There is support for editor highlighting to aid development. Follow these steps to enable highlighting in Visual Studio Code (see [here](https://www.npmjs.com/package/codelyzer#editor-configuration) for other editors)

* Install the `tslint` plugin
* Add the following to your `File > Preferences > Settings` configuration:

```
{
    "tslint.rulesDirectory": "./node_modules/codelyzer",
    "typescript.tsdk": "node_modules/typescript/lib"
}
```


## Deploying ##

* Run `ng build --prod`
* Copy all contents of the `dist` folder generated in your repository folder to web server of your choice
* For IIS deployment:
* * Ensure you have the [URL Rewrite](https://www.iis.net/downloads/microsoft/url-rewrite) extension installed
* * Ensure that there is a `web.config` in the `dist` folder. If not, create one with the following settings:
* * * ```
        <rewrite>
            <rules>
                <rule name="Ignore Static Files" enabled="true" stopProcessing="true">
                    <match url="(css/.*|img/.*|js/.*|lib/.*|partials/.*|index.html)" />
                    <action type="None" />
                </rule>
                <rule name="Redirect Page Navigation" enabled="true" stopProcessing="true">
                    <match url=".*" />
                    <conditions logicalGrouping="MatchAll">
                        <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
                        <!--<add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />-->
                        <add input="{REQUEST_URI}" pattern="^/(bundles)" negate="true" />
                    </conditions>
                    <action type="Rewrite" url="/index.html" />
                </rule>
            </rules>
        </rewrite>
      ```
* * Open the web app port in the firewall

## Angular CLI Features ###

### Development server
Run `ng serve --port 4200` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
