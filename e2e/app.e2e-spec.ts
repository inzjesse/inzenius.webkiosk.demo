import { InzeniusCloudPage } from './app.po';

describe('inzenius-cloud App', function() {
  let page: InzeniusCloudPage;

  beforeEach(() => {
    page = new InzeniusCloudPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
