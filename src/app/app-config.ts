import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

export interface IAppConfig {
    api: string;
    paymasterEmailAddress: string;
}

@Injectable()
export class AppConfig {

    public static settings: IAppConfig;

    constructor(private http: HttpClient) {}

    load() {
        const jsonFile = `assets/global-config.json`;
        return this.http.get(jsonFile).pipe(tap((config: IAppConfig) => AppConfig.settings = config)).toPromise();
    }
}
