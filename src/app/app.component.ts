import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import * as wjcCore from 'wijmo/wijmo';

import { DataService } from './data.service';
import { AppConfig } from './app-config';
import { Login } from './interfaces/login';

wjcCore.setLicenseKey('394812491327591#B0bPtJ4ZwwEUXV4MFF6RSFzNqhFZntkZvdlSpl7dLNDNahGTjhHUsJjQLBVdS34LXJFZ5I6YhZjWrkTWtBleYtkdn9UQGd7UxAVR6NDd5wGaUZ4boZEcyFWZvF6SpZEMQtkQrh5d8hGNXdGRol4UHNjd64WO9hTeZJUNZx4UQ3yTMxUMrNDMqV7LOR4S8QEd7kDTqRTOnNmNKl4bSRmesVkVRlTQFRmajVndrBjNDVVa9hjQsNXNvc6YwJVboNlQxpESzdDNNJkWjticF5UbtV4L9UkSklXcX36MGR7QIdkVZZkSyFDOyk6SqN7Q7dDc7dlWsVERCx4a8t6cUZFMmVzLtZUSrIGepFUWl36Umh7bCNmT9JkbMp4ZBFzTGd6TkJ5YaplYoR7Ly3CesxmQzhHd456RyVTbjZmMspnSv8mdwU5QCx4MaR5Npx6QQZmRy9GaTtGUJJHViojITJCLicjQzUERDVUMiojIIJCL4gzM8YDNzAjN0IicfJye#4Xfd5nIJBjMSJiOiMkIsIibvl6cuVGd8VEIgQXZlh6U8VGbGBybtpWaXJiOi8kI1xSfiUTSOFlI0IyQiwiIu3Waz9WZ4hXRgAicldXZpZFdy3GclJFIv5mapdlI0IiTisHL3JyS7gDSiojIDJCLi86bpNnblRHeFBCI73mUpRHb55EIv5mapdlI0IiTisHL3JCNGZDRiojIDJCLi86bpNnblRHeFBCIQFETPBCIv5mapdlI0IiTisHL3JyMDBjQiojIDJCLiUmcvNEIv5mapdlI0IiTisHL3JSV8cTQiojIDJCLi86bpNnblRHeFBCI4JXYoNEbhl6YuFmbpZEIv5mapdlI0IiTis7W0ICZyBlIsIyNwIzMxADI9ATNwgTMwIjI0ICdyNkIsIyc5lmblpnbJJiOiEmTDJCLiETO5cjMzETO4ITM8QTOzIiOiQWSiwSfiEjd8EDMyIiOiIXZ6JCLlNHbhZmOiI7ckJye0ICbuFkI1pjIEJILc0'); // tslint:disable-line

const { version: appVersion } = require('../../package.json');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {

  _router: Router;
  _dataService: DataService;
  appVersion: string;
  userName: string;
  employeeDisplayName: string;

  constructor(_router: Router, _dataService: DataService) {
    this._router = _router;
    this._dataService = _dataService;
    this.appVersion = appVersion;
    this.userName = _dataService.getUser();
    this.employeeDisplayName = 'Allan Alder (111)';

    this._dataService.loggedIn.subscribe((login: Login) => {
      this.userName = login.username;
      this._dataService.refreshUserReportTypes();
      this._dataService.saveUser(login.username);
      this.employeeDisplayName = login.employeeDisplayName;
    });
  }

  logout() {
    localStorage.removeItem('auth');
    this._router.navigate(['login']);
    localStorage.removeItem('username');
  }

  ngOnInit() {
    if (this._dataService.isLoggedIn()) {
      this._dataService.refreshUserReportTypes();
    }
  }
}
