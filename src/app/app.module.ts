import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { WjCoreModule } from 'wijmo/wijmo.angular2.core';
import { WjGridModule } from 'wijmo/wijmo.angular2.grid';
import { WjInputModule } from 'wijmo/wijmo.angular2.input';
import { WjNavModule } from 'wijmo/wijmo.angular2.nav';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { AppComponent } from './app.component';
import { AppConfig } from './app-config';
import { DataService } from './data.service';
import { routing } from './app.routing';
import { LoginComponent } from './login/login.component';
import { RosteringComponent } from './rostering/rostering.component';
import { AuthGuardService } from './auth-guard.service';
import { TreeComponent } from './tree/tree.component';
import { TreeViewComponent } from './tree/tree-view/tree-view.component';
import { RosterComponent } from './rostering/roster/roster.component';
import { ReportComponent } from './report/report.component';
import { ReportTreeComponent } from './report-tree/report-tree.component';
import { LogComponent } from './rostering/log/log.component';
import { RosterToolbarComponent } from './rostering/roster-toolbar/roster-toolbar.component';
import { ContextMenuComponent } from './rostering/roster/context-menu/context-menu.component';
import { Commands } from './commands';
import { BaseReportComponent } from './reports/base-report';
import { ProfileOptionsComponent } from './profile-options/profile-options.component';
import { ChangePasswordComponent } from './profile-options/change-password/change-password.component';
import { LoctimesheetReportComponent } from './reports/loctimesheet-report/loctimesheet-report.component';
import { EmptimesheetReportComponent } from './reports/emptimesheet-report/emptimesheet-report.component';
import { ViewerReportComponent } from './reports/viewer-report/viewer-report.component';
import { FilterReportComponent } from './reports/report-settings/filter-report/filter-report.component';
import { Reportcommands } from './reportcommands';
import { PaginationService} from './pagination.service';
import { SearchPipe } from './search.pipe';
import { RosterGridComponent } from './roster-grid/roster-grid.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeePayslipsComponent } from './employee-payslips/employee-payslips.component';
import { EmployeeLeaveComponent } from './employee-leave/employee-leave.component';
import { EmployeeLeaveRequestComponent } from './employee-leave/employee-leave-request/employee-leave-request.component';
import { EmployeeLeaveDetailsComponent } from './employee-leave/employee-leave-details/employee-leave-details.component';
import { EmployeePayslipViewComponent } from './employee-payslips/employee-payslip-view/employee-payslip-view.component';

export function loadAppConfig(config: AppConfig) {
  return () => config.load();
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RosteringComponent,
    TreeComponent,
    TreeViewComponent,
    RosterComponent,
    ReportComponent,
    ReportTreeComponent,
    LogComponent,
    RosterToolbarComponent,
    ContextMenuComponent,
    BaseReportComponent,
    ProfileOptionsComponent,
    ChangePasswordComponent,
    LoctimesheetReportComponent,
    EmptimesheetReportComponent,
    ViewerReportComponent,
    FilterReportComponent,
    SearchPipe,
    RosterGridComponent,
    EmployeeDetailsComponent,
    EmployeePayslipsComponent,
    EmployeeLeaveComponent,
    EmployeeLeaveRequestComponent,
    EmployeeLeaveDetailsComponent,
    EmployeePayslipViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    routing,
    WjCoreModule,
    WjGridModule,
    WjInputModule,
    WjNavModule,
    BsDropdownModule.forRoot()
  ],
  providers: [
    AppConfig,
    { provide: APP_INITIALIZER, useFactory: loadAppConfig, deps: [AppConfig], multi: true },
    DataService,
    PaginationService,
    AuthGuardService,
    Commands,
    Reportcommands
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
