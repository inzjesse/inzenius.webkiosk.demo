import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RosteringComponent } from './rostering/rostering.component';
import { AuthGuardService } from './auth-guard.service';
import { ReportComponent } from './report/report.component';
import { ReportRoutes } from './report/report.routing';
import { ProfileOptionsComponent } from './profile-options/profile-options.component';
import { ChangePasswordComponent } from './profile-options/change-password/change-password.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeLeaveComponent } from './employee-leave/employee-leave.component';
import { EmployeePayslipsComponent } from './employee-payslips/employee-payslips.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'rostering', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuardService] },
  { path: 'rostering', component: RosteringComponent, canActivate: [AuthGuardService] },
  { path: 'profile-options', component: ProfileOptionsComponent, canActivate: [AuthGuardService] },
  { path: 'my-details', component: EmployeeDetailsComponent, canActivate: [AuthGuardService] },
  { path: 'leave', component: EmployeeLeaveComponent, canActivate: [AuthGuardService] },
  { path: 'payslips', component: EmployeePayslipsComponent, canActivate: [AuthGuardService] },
  { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuardService] },
  ...ReportRoutes
];

export const routing = RouterModule.forRoot(appRoutes);
