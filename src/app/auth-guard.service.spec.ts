import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { AuthGuardService } from './auth-guard.service';
import { DataServiceStub } from '../testing/helpers';
import { DataService } from './data.service';

describe('Service: AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [
        AuthGuardService,
        { provide: DataService, useClass: DataServiceStub },
      ]
    });
  });

  it('should redirect to login',
    async(inject([AuthGuardService, DataService, Router ], (auth, dataService, router) => {
      // spy on router nagivation
      spyOn(router, 'navigate');

      // spy on data service calls
      spyOn(dataService, 'isLoggedIn').and.callFake(() => {
        return false;
      });
      spyOn(dataService, 'canAccessReports').and.callFake(() => {
        return false;
      });

      expect(auth.canActivate('', '/reports')).toBeFalsy();
      expect(router.navigate).toHaveBeenCalledWith(['login']);
    })
  ));
});
