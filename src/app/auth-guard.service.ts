import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { DataService } from './data.service';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

    constructor(private _dataService: DataService, private _router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // for login
        if (state.url === '/login') {
            if (this._dataService.isLoggedIn()) {
                this._router.navigate(['rostering']);
                return false;
            }
            return true;
        }

        // for reports - check for user role permissions
        if (state.url === '/reports') {
            if (this._dataService.isLoggedIn() && this._dataService.canAccessReports()) {
                return true;
            }

            // no report permissions
            this._router.navigate(['rostering']); // redirect to rostering
            return false;
        }

        // for every other page
        if (this._dataService.isLoggedIn()) { return true; }
        this._router.navigate(['login']);
        return false;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.canActivate(route, state);
    }

}
