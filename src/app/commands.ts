import { Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import * as wjcGrid from 'wijmo/wijmo.grid';
import * as moment from 'moment';

import { DataService } from './data.service';
import { ExistingRoster } from './interfaces/existing-roster';

export class Commands {

  _dataService: DataService;

  acceptRosterCommand = {
    canExecuteCommand: () => {
      const selectedRoster: ExistingRoster = this._dataService.rosterGrid.selectedItems[0];
      return (this._dataService.rosterGrid.selectedItems.length === 1 &&
        selectedRoster.rosterAcceptanceTypeId === 2 &&
        selectedRoster.rosterTypeId === 1); // pre-published roster
    },
    executeCommand: () => {
      this._dataService.loading = true;
      const selectedRosterId: number = this._dataService.rosterGrid.selectedItems[0].rosterId;
      this._dataService.acceptRejectRoster(selectedRosterId, true).subscribe(() => {
        this._dataService.showRosters.emit(this._dataService.filterData); // refresh grid
      }, error => {
        this._dataService.loading = false;
      });
    }
  };

  rejectRosterCommand = {
    canExecuteCommand: () => {
      const selectedRoster: ExistingRoster = this._dataService.rosterGrid.selectedItems[0];
      return (this._dataService.rosterGrid.selectedItems.length === 1 &&
        selectedRoster.rosterAcceptanceTypeId === 2 &&
        selectedRoster.rosterTypeId === 1); // pre-published roster
    },
    executeCommand: () => {
      this._dataService.loading = true;
      const selectedRosterId: number = this._dataService.rosterGrid.selectedItems[0].rosterId;
      this._dataService.acceptRejectRoster(selectedRosterId, false).subscribe(() => {
        this._dataService.showRosters.emit(this._dataService.filterData); // refresh grid
      }, error => {
        this._dataService.loading = false;
      });
    }
  };

  workingCommand = {
    executeCommand: () => {
      this.publishRosterCommand.executeCommand('btnWorking');

    },
    canExecuteCommand: () => {
      return !this._dataService.disableWorkingEng;
   }
  };

  prePublishCommand = {
    executeCommand: () => {
      this.publishRosterCommand.executeCommand('btnPrePublish');

    },
    canExecuteCommand: () => {
      return !this._dataService.disablePrepublishedgEng;
   }
  };

  publishCommand = {
    executeCommand: () => {
      this.publishRosterCommand.executeCommand('btnPublish');

    },
    canExecuteCommand: () => {
       return !this._dataService.disablePublishedEng;
    }
  };

  publishRosterCommand = {
    executeCommand: (clickType: string) => {
      let rosterAcceptanceType: number;
      this._dataService.pbChangedToWorking = false;
      switch (clickType) {
        case 'btnWorking':
          rosterAcceptanceType = 1;
          this._dataService.pbChangedToWorking = true;
          break;
        case 'btnPrePublish':
          rosterAcceptanceType = 2;
          break;
        case 'btnPublish':
          rosterAcceptanceType = 3;
          break;
      }
      this._dataService.loading = true;
      // prepare status change for each selected roster
      const publishRequests: Observable<any>[] = [];
      const employeeDetails: any[] = [];
      this._dataService.rosterGrid.selectedRows.forEach((row: wjcGrid.Row) => {
        if (row.dataItem.rosterAcceptanceTypeId !== rosterAcceptanceType) {
          // lazy load the API observable
          publishRequests.push(this._dataService.publishRoster(row.dataItem.rosterId, rosterAcceptanceType));
          if (!employeeDetails.includes(row.dataItem.employeeName) && row.dataItem.rosterAcceptanceTypeId === 3) {
            employeeDetails.push(row.dataItem.employeeName); // only give warning messages for employee changing from Pb to working state.
          }
        }
      });

      this._dataService.logMessage.emit(`Changing status of ${publishRequests.length} engagement${publishRequests.length > 1 ? 's' : ''}.`);

      forkJoin(...publishRequests).subscribe(results => {
        const Date = results[0].RosterRecord.RosterStartDate;
        this._dataService.loading = false;
        this._dataService.logMessage.emit(`Roster status changed for ${results.length} engagement${results.length > 1 ? 's' : ''}.`);
        if (this._dataService.pbChangedToWorking === true && results.length > 0 && employeeDetails.length > 0) {
          this._dataService.logMessage.emit(`Please contact the following employee(s) that the Roster has been changed `
                                            + `to Working status: ${employeeDetails}.`);
        }
        // reload roster grid
        if (this._dataService.filterData) {
          this._dataService.showRosters.emit(this._dataService.filterData);
        }
      }, error => {
        error = error.error;
        let errMsg = 'Unable to change roster status.';
        console.error(error);
        this._dataService.loading = false;
        if (error.description && error.errors.length) {
            errMsg += ` ${error.description}`;
            errMsg += ` ${error.errors}`;
        }
        this._dataService.logMessage.emit(errMsg);
         // reload roster grid
         if (this._dataService.filterData) {
          this._dataService.showRosters.emit(this._dataService.filterData);
        }
      });

    },
    canExecuteCommand: () => this._dataService.rosterGrid.selectedRows.length
  };

  constructor(@Inject(DataService) _dataService: DataService) {
    this._dataService = _dataService;
  }
}
