import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DataService } from './data.service';

describe('Service: Data', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [ DataService ]
    });

    // inject http service and test controller
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);

    // spy on locaStorage calls for token auth
    let store = {};
    spyOn(localStorage, 'getItem').and.callFake(function(key) {
      return store[key] ? store[key] : null;
    });
    spyOn(localStorage, 'setItem').and.callFake(function(key, value) {
      return store[key] = value + '';
    });
    spyOn(localStorage, 'clear').and.callFake(function() {
      store = {};
    });
  });

  it('should emit from showAddEngagementPopup', inject([DataService], (service: DataService) => {
    service.showAddEngagementPopup.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.showAddEngagementPopup.emit(true);
  }));

  it('should emit from showRosters', inject([DataService], (service: DataService) => {
    service.showRosters.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.showRosters.emit(true);
  }));

  it('should emit from showReport', inject([DataService], (service: DataService) => {
    service.showReport.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.showReport.emit(true);
  }));

  it('should emit from showfilterReportPopup', inject([DataService], (service: DataService) => {
    service.showFilterReportPopup.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.showFilterReportPopup.emit(true);
  }));


  it('should emit from showreportLoadedtoViewer', inject([DataService], (service: DataService) => {
    service.showReportLoadedToViewer.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.showReportLoadedToViewer.emit(true);
  }));

  it('should emit from reportFilterChanged', inject([DataService], (service: DataService) => {
    service.reportFilterChanged.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.reportFilterChanged.emit(true);
  }));
  it('should emit from reportPageChanged', inject([DataService], (service: DataService) => {
    service.reportPageChanged.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.reportPageChanged.emit(true);
  }));

  it('should emit from reportPrint', inject([DataService], (service: DataService) => {
    service.reportPrint.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.reportPrint.emit(true);
  }));
  it('should emit from reportDownload', inject([DataService], (service: DataService) => {
    service.reportDownload.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.reportDownload.emit(true);
  }));
  it('should emit from reportExcelExport', inject([DataService], (service: DataService) => {
    service.reportExcelExport.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.reportExcelExport.emit(true);
  }));
  it('should emit from refreshViewer', inject([DataService], (service: DataService) => {
    service.refreshViewer.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.refreshViewer.emit(true);
  }));

  it('should emit from logMessage', inject([DataService], (service: DataService) => {
    service.logMessage.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.logMessage.emit('true');
  }));

  it('should inject service', inject([DataService], (service: DataService) => {
    expect(service).toBeTruthy();
  }));

  it('should not be logged in after init (with cleared localStorage)', inject([DataService], (service: DataService) => {
    localStorage.clear();
    expect(service.isLoggedIn()).toBeFalsy();
  }));

  it('should retrieve saved token', inject([DataService], (service: DataService) => {
    const token = 'testtoken';
    service.saveToken(token);
    expect(service.getToken()).toEqual(token);
  }));

  it('should not have expired token', inject([DataService], (service: DataService) => {
    const token = 'testtoken';
    service.saveToken(token);
    expect(service.isTokenExpired).toBeTruthy();
  }));

  it('should emit from showSuppGrid', inject([DataService], (service: DataService) => {
    service.showSuppGrid.subscribe(res => {
      expect(res).toBeTruthy();
    });
    service.showSuppGrid.emit(true);
  }));
});
