import { Injectable , Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcGrid from 'wijmo/wijmo.grid';
import * as wjcInput from 'wijmo/wijmo.input';

import { Login } from './interfaces/login';
import { EntityType } from './interfaces/entity-type';
import { DLICosting } from './interfaces/dli-costing';
import { TreeEntity } from './interfaces/tree-entity';
import { ExistingRoster } from './interfaces/existing-roster';
import { UserOptions } from './interfaces/user-options';
import { RosterEntity } from './interfaces/roster-entity';
import { CreatedRoster } from './interfaces/created-roster';
import { EmployeeEntity } from './interfaces/employee-entity';
import { ReportType } from './interfaces/report-type';
import { SiteInactivityResponse } from './interfaces/site-inactivity';
import { UtilisationReportResponse } from './interfaces/utilisation';
import { RosterReport } from './interfaces/rosterreport';
import { EmployeeRosterByPeriod } from './interfaces/employee-roster-by-period';
import { ReportSettings } from './interfaces/report-settings';
import { EmployeeDetails } from './employee-details/employee-details.component';
import { EmployeeLeave } from './employee-leave/employee-leave.component';
import { AppConfig } from './app-config';
import { LeaveRequest } from './employee-leave/employee-leave-request/employee-leave-request.component';

@Injectable()
export class DataService {

  @Output() showRosters = new EventEmitter<any>();
  @Output() showReport = new EventEmitter<any>();
  @Output() logMessage = new EventEmitter<string>();
  @Output() showRosterTimes = new EventEmitter<any>();
  @Output() showPreviousPeriod = new EventEmitter<any>();
  @Output() showNextPeriod = new EventEmitter<any>();
  @Output() changeDataOption = new EventEmitter<any>();
  @Output() showAddEngagementPopup = new EventEmitter<any>();
  @Output() loggedIn = new EventEmitter<any>();
  @Output() showFilterReportPopup = new EventEmitter<any>();
  @Output() showReportLoadedToViewer = new EventEmitter<any>();
  @Output() reportFilterChanged = new EventEmitter<any>();
  @Output() reportPageChanged = new EventEmitter<any>();
  @Output() reportSearchText = new EventEmitter<any>();
  @Output() reportPrint = new EventEmitter<any>();
  @Output() reportDownload = new EventEmitter<any>();
  @Output() reportExcelExport = new EventEmitter<any>();
  @Output() refreshViewer = new EventEmitter<any>();
  @Output() showSuppGrid = new EventEmitter<any>();
  @Output() changeColumnOption = new EventEmitter<any>();
  @Output() reportZoomChanged = new EventEmitter<number>();


  loading = false;
  apiBaseUrl = 'http://192.168.99.201:8085/v1'; //AppConfig.settings.api; //localStorage.getItem('currentConnection');
  gridData = new wjcCore.CollectionView();
  isAuthorizable = false;
  selectedItem: {} = null;
  filterData = null;
  rosterGrid: wjcGrid.FlexGrid;
  copiedRosters: {} = null;
  addNewEngagementPopup: wjcInput.Popup;
  reportGrid: wjcGrid.FlexGrid;
  reportSummaryGrid: wjcGrid.FlexGrid;
  userReportTypes: any[] = [];
  convertEngagementPopup: wjcInput.Popup;
  isAddEngagement = false;
  pbChangedToWorking = false;
  disableWorkingEng = false;
  disablePrepublishedgEng = false;
  disablePublishedEng = false;
  selectedReport = 'Select Report';
  filterReportPopup: wjcInput.Popup;
  reportSettingsFilter: ReportSettings;
  enableToolbar = false;
  showEmployeeGrid = false;
  employeeGridData = new wjcCore.CollectionView();
  suppRosterGridData = new wjcCore.CollectionView();
  suppRosterGrid: wjcGrid.FlexGrid;
  supplementaryGridEntity = 0;
  loadingEmployeeRosters = false;
  rosterOptionsPopup: wjcInput.Popup;

  constructor(private _http: HttpClient) {
    this.apiBaseUrl = AppConfig.settings.api;
  }

  login(username: string, password: string): Observable<Login> {
    // return Observable.of({
    //   token: 'abc123',
    //   claims: ['manager', 'user'],
    //   username: username,
    //   employeeDisplayName: 'Allan Alder (123)'
    // }); // TODO
    const body = JSON.stringify({ username, password });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<Login>(this.apiBaseUrl + '/login', body, httpOptions);
  }

  saveToken(token: string) {
    // token expires after 24 hours
    const expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + 1);

    const obj = {
      token: token,
      expires: expirationDate
    };
    localStorage.setItem('auth', JSON.stringify(obj));
  }

  getToken() {
    let authData = localStorage.getItem('auth');
    authData = JSON.parse(authData);
    return authData ? authData['token'] : null;
  }

  saveUser(username: string) {
    localStorage.setItem('username', username);
  }

  getUser() {
    const userName = localStorage.getItem('username');
    return userName;
  }

  isLoggedIn() {
    if (this.getToken()) { return !this.isTokenExpired(); }
    return false;
  }

  isTokenExpired() {
    let authData = localStorage.getItem('auth');
    authData = JSON.parse(authData);

    let expirationDate = authData['expires'];
    expirationDate = new Date(expirationDate);

    const currentDate = new Date();

    return currentDate > expirationDate;
  }

  // return true if the user has permissions to view a specific report type
  canViewReport(reportType: string): boolean {
    for (const report of this.userReportTypes) {
      if (report.reportType === reportType) {
        return true;
      }
    }
    return false;
  }

  // return true if the user has permissions to view AT LEAST one report
  canAccessReports() {
    return this.userReportTypes.length > 0;
  }

  // load and save the report types that the user has access to (async call)
  refreshUserReportTypes() {
    this.getReportTypes().subscribe((reports) => {
      this.userReportTypes = reports;
    }, error => {
      this.userReportTypes = [];
    });
  }

  getEntityTypes(): Observable<EntityType[]> {
    return Observable.of([{ entityName: 'Cost Centre'}, {entityName: 'Employee'}]);

    // const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    // const httpOptions = { headers: httpHeaders };

    // return this._http.get<Array<EntityType>>(this.apiBaseUrl + '/entities/functions', httpOptions);
  }

  getTreeEntities(entityType: string, dateFrom: string, dateTo: string): Observable<TreeEntity[]> {
    return Observable.of([
      {entityBold: 1,
        entityDisabled: 0,
        entityId: '1',
        entityLevel: 0,
        entityName: 'test',
        entityParentId: '0',
        entitySequence: 0,
        entityType: 1},
      {entityBold: 1,
        entityDisabled: 0,
        entityId: '2',
        entityLevel: 0,
        entityName: 'test',
        entityParentId: '0',
        entitySequence: 1,
        entityType: 1}
    ]);
    // const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    // const httpOptions = { headers: httpHeaders };

    // return this
    //         ._http
    //         .get<Array<TreeEntity>>(this.apiBaseUrl + '/entities?type=tree&dateFrom=' + dateFrom + '&dateTo=' + dateTo
    //                                                 + '&entityType=' + entityType
    //                                                 + '&checkRostered=true', httpOptions);
  }

  getReportTypes(): Observable<ReportType[]> {
    // return Observable.of([{ reportType: 'test', reportName: 'Test Report' }]); // TODO
    const testReports: ReportType[] = [
      // { reportName: 'Attendance Report', reportType: 'attendance' },
      // { reportName: 'Timesheet By Location Report', reportType: 'timesheetbylocationreport' },
      { reportName: 'Timesheet Report', reportType: 'timesheetbyemployeereport' }
    ];
    return Observable.of(testReports);

    // const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    // const httpOptions = { headers: httpHeaders };

    // return this._http.get<Array<ReportType>>(this.apiBaseUrl + '/entities/reports', httpOptions);
  }

  getUserOptions(): Observable<UserOptions> {
    const testOptions: UserOptions = {
      allowance: true,
      asset: true,
      autoSelectEntities: true,
      breakEnd: true,
      breakPaid: true,
      breakStart: true,
      costCentre: true,
      date: true,
      deduction: true,
      employeeName: false,
      events: true,
      finishTime: true,
      grade: true,
      location: true,
      notes: true,
      notesHotkey: true,
      popupMessages: true,
      position: true,
      rosterAcceptance: true,
      rosterOrder: 1,
      rule: true,
      shiftComposition: true,
      startTime: true,
      task: true,
      timeFormat: 11, // 'HH:mm'
      total: true,
      units: true
    };
    return Observable.of(testOptions); // TODO

    // const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    // const httpOptions = { headers: httpHeaders };

    // return this._http.get<UserOptions>(this.apiBaseUrl + '/users/options', httpOptions);
  }

  updateUserOption(param: string, value: boolean): Observable<UserOptions> {
    const postData = {};
    postData[param] = value;
    const body = JSON.stringify(postData);
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<UserOptions>(this.apiBaseUrl + '/users/options', body, httpOptions);
  }

  getRosters(entitiesFilter: string, entityType: string,
             dateFrom: string, dateTo: string, employeeId?: number): Observable<ExistingRoster[]> {
    const test: ExistingRoster = {
        rosterId: 0,
        shiftId: 0,
        rosterTypeId: 1,
        employeeName: 'Testy McTesterson',
        employeeId: 0,
        shiftCompositionCode: 'Pxx',
        shiftEntityTypeId: 0,
        startDate: 'Thu 01 Jan',
        startDateFull: new Date('2015-01-01T00:00:00.0000000+11:00'),
        startTime: '15:30',
        startTimeFull: new Date('2015-01-01T15:30:00.0000000+11:00'),
        endTime: '23:36',
        endTimeFull: new Date('2015-01-01T23:36:00.0000000+11:00'),
        breakStart: '19:18',
        breakStartFull: new Date('2015-01-01T19:18:00.0000000+11:00'),
        breakEnd: '19:48',
        breakEndFull: new Date('2015-01-01T19:48:00.0000000+11:00'),
        breakPaid: false,
        breakLength: 0,
        engagementLength: '7:36',
        engagementLengthFull: 456,
        locationName: 'Testing',
        locationId: 0,
        gradeName: '',
        gradeId: 0,
        ruleName: '',
        ruleIds: [
           0
        ],
        costCentreName: 'Test Centre',
        costCentreId: 0,
        eventName: '',
        eventId: 0,
        positionName: 'SRS Grade FT 1',
        positionId: 0,
        taskIds: [
           0
        ],
        taskName: '',
        assetIds: [
           0
        ],
        assetName: '',
        allowanceIds: [
           0
        ],
        allowanceDetails: '',
        deductionIds: [
           0
        ],
        deductionDetails: '',
        rosterNotes: '',
        isSigned: false,
        rosterAcceptanceTypeDisplay: 'W',
        rosterAcceptanceTypeId: 1,
        shiftCompositionCodeDisplay: 'P'
        // signatureContent: ''
    };
    const rosters: ExistingRoster[] = [];
    for (let i = 1; i < 30; i++) {
      test.shiftId = Math.round(i / 3);
      test.rosterId = i;
      test.rosterTypeId = (i % 2) + 1;
      test.startTimeFull.setMinutes(test.startTimeFull.getMinutes() + i);
      test.endTimeFull.setMinutes(test.endTimeFull.getMinutes() + i);
      rosters.push({...test});
    }
    return Observable.of(rosters); // TODO

    // const entityFilterIDlist: string[] = [entitiesFilter];
    // let postData;
    // if (employeeId) {
    //    postData = { 'entitiesFilter': entityFilterIDlist, dateFrom, dateTo, entityType, employeeId };
    // } else {
    //    postData = { 'entitiesFilter': entityFilterIDlist, dateFrom, dateTo, entityType };
    // }
    // const body = JSON.stringify(postData);
    // const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
    //                                 'Content-Type': 'application/json' });
    // const httpOptions = { headers: httpHeaders };

    // return this._http.post<Array<ExistingRoster>>(this.apiBaseUrl + '/rosters/load', body, httpOptions);
  }

  getEmployeeRosters(entitiesFilter: string,
                     entityType: string,
                     dateFrom: string,
                     dateTo: string,
                     employeeId?: number): Observable<ExistingRoster[]> {
    const entityFilterIDlist: string[] = [entitiesFilter];
    let postData;
    if (employeeId) {
    postData = { 'entitiesFilter': entityFilterIDlist, dateFrom, dateTo, entityType, employeeId };
    } else {
    postData = { 'entitiesFilter': entityFilterIDlist, dateFrom, dateTo, entityType };
    }
    const body = JSON.stringify(postData);
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<Array<ExistingRoster>>(this.apiBaseUrl + '/rosters/load', body, httpOptions);
}

  getRostersForEmployeeByPeriod(employeeId: number,
                                dateFrom: string,
                                dateTo: string,
                                entityTypeId: number): Observable<EmployeeRosterByPeriod[]> {
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    const httpOptions = { headers: httpHeaders };
    const url = this.apiBaseUrl + '/rostersforempbyperiod?employeeId=' + employeeId + '&dateFrom=' + dateFrom
              + '&dateTo=' + dateTo + '&entityTypeId=' + entityTypeId;

    return this._http.get<Array<EmployeeRosterByPeriod>>(url, httpOptions);
  }

  updateRoster(rosterId: number, data: any[]) {
    const postData = { rosterId };

    for (const param of data) {
      postData[param.paramName] = param.paramValue;
    }

    const body = JSON.stringify(postData);
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post(this.apiBaseUrl + '/rosters', body, httpOptions);
  }

  deleteRoster(rosterId: number) {
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    const httpOptions =  {headers: httpHeaders };

    return this._http.delete(this.apiBaseUrl + `/rosters/${rosterId}`, httpOptions);
  }

  splitShift(rosterId: number) {
    const body = JSON.stringify({ rosterId });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post(this.apiBaseUrl + `/rosters/splitshift/${rosterId}`, body, httpOptions);
  }

  mergeShift(rosterIdList: string) {
    const body = JSON.stringify({ rosterIdList });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                      'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post(this.apiBaseUrl + '/rosters/mergeshift', body , httpOptions);
  }

  addAnotherShift(entityId: number, entityType: string, rosterId: number) {
    const body = JSON.stringify({ entityId, entityType, rosterId });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post(this.apiBaseUrl + `/rosters/addanothershift`, body, httpOptions);
  }

  getRosterEntities(entityType: string, rosterId: number): Observable<RosterEntity[]> {
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    const httpOptions = { headers: httpHeaders };

    return this._http.get<Array<RosterEntity>>(this.apiBaseUrl + '/entities/' + entityType + '?rosterId=' + rosterId, httpOptions);
  }

  authoriseRoster(rosterId: number): Observable<{rosterId: number}> {
    const body = JSON.stringify({ rosterId });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<{rosterId: number}>(this.apiBaseUrl + '/rosters/authorise', body, httpOptions);
  }

  createRoster(employeeId: number, entityType: string,
               rosterType: string, rosterDate: string, entityFilterID: number): Observable<CreatedRoster> {
    const body = JSON.stringify({ employeeId, entityType, rosterType, rosterDate, entityFilterID });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<CreatedRoster>(this.apiBaseUrl + '/rosters/create', body, httpOptions);
  }

  convertRoster(rosterId: number, targetEntityId: number, targetEntityType: string) {
    const body = JSON.stringify({ rosterId, targetEntityId, targetEntityType});
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post(this.apiBaseUrl + '/rosters/convertshift', body, httpOptions);
  }
  getRosterReports(rosterIdList: number[]) {
    const postData = { rosterIdList };
    const body = JSON.stringify(postData);
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json'});
    const httpOptions = { headers: httpHeaders };

    return this._http.post<Array<RosterReport>>(this.apiBaseUrl + '/reports/rosterreport', body, httpOptions);
  }

  getEmployeeList(dateFrom: string, dateTo: string, entityType: string, entityFilterID: number): Observable<EmployeeEntity[]> {
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    const httpOptions = { headers: httpHeaders };

    return this
            ._http
            .get<Array<EmployeeEntity>>(this.apiBaseUrl
                                          + `/entities/EmployeeList?dateFrom=${dateFrom}&dateTo=${dateTo}&entityType=${entityType}`
                                          + `&entityFilterID=${entityFilterID}`, httpOptions);
  }

  getEmployeeFilteredList(dateFrom: string, dateTo: string, qualificationIds: string, positionIds: string, costcenterIds: string,
                          gradeIds: string, leaveIds: string, locationIds: string): Observable<TreeEntity[]> {
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    const httpOptions = { headers: httpHeaders };

    return this
            ._http
            .get<Array<TreeEntity>>(this.apiBaseUrl + `/entities/EmployeeFilteredList?dateFrom=${dateFrom}&dateTo=${dateTo}`
                                    + `&qualificationIds=${qualificationIds}&positionIds=${positionIds}&costcentreIds=${costcenterIds}`
                                    + `&gradeIds=${gradeIds}&leaveIds=${leaveIds}&locationIds=${locationIds}`, httpOptions);
  }

  getReportData(dateFrom: string, dateTo: string, entityType: string, entityFilterID: number) {
    const test = {
      employeeName: 'Alder, Alan Mr (111)',
      employeeLastName: 'Alder',
      employeeFirstName: 'Alan',
      employeeSalutation: 'Mr',
      employeeNumber: '111',
      startDate: '2016-11-02T00:00:00.0000000+11:00',
      startTime: '2016-11-02T09:00:00.0000000+11:00',
      endTime: '2016-11-02T17:30:00.0000000+11:00',
      breakStart: '13:00',
      breakStartFull: '2016-11-02T13:00:00.0000000+11:00',
      breakEndFull: '2016-11-02T13:30:00.0000000+11:00',
      breakEnd: '13:30',
      breakPaid: true,
      breakLength: 30,
      locationName: '313 Allied Health',
      gradeName: 'Canberra',
      costCentreName: 'Operations',
      eventName: 'Event 201312',
      positionName: 'Superivisor - Admin',
      taskName: 'Security Mobile',
      assetName: '',
      rosterTypeId: 1,
      shiftId: 40357,
      rosterID: 112694,
      rosterNotes: 'test2',
      isSelected: true,
      employeeID: 2,
      shiftEntityTypeId: 1,
      allowanceDetails: '',
      allowanceAmount: '',
      rosterStartDate: '2016-11-02T00:00:00.0000000+11:00',
      isBreakPaid: true,
      breakDuration: 30,
      employeeStr: 'Alder, Alan Mr (111)'
    };
    const rosters = [];
    for (let i = 1; i < 30; i++) {
      test.shiftId = i;
      // test.rosterId = i * 15;
      test.rosterTypeId = (i % 2) + 1;
      // test.startTime.setMinutes(test.startTime.getMinutes() + i);
      // test.endTime =.setMinutes(test.endTime.getMinutes() + i);
      rosters.push({...test});
    }

    return Observable.of(rosters);

    // const entityFilterIDlist: number[] = [entityFilterID];
    // const postData = { dateFrom, dateTo, entityType, 'entityFilterID': entityFilterIDlist };
    // const body = JSON.stringify(postData);
    // const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
    //                                 'Content-Type': 'application/json'});
    // const httpOptions = { headers: httpHeaders };

    // return this._http.post<Array<ExistingRoster>>(this.apiBaseUrl + '/reports/rosters', body, httpOptions);
  }

  appendPrepend(rosterId: number, appendEngagement?: boolean): Observable<CreatedRoster> {
    const body = JSON.stringify({ rosterId, appendEngagement });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<CreatedRoster>(this.apiBaseUrl + '/rosters/appendprepend', body, httpOptions);
  }

  publishRoster(rosterId: number, rosterAcceptanceTypeId: number) {
    const body = JSON.stringify({ rosterId, rosterAcceptanceTypeId });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<CreatedRoster>(this.apiBaseUrl + '/rosters/publish', body, httpOptions);
  }

  copyPaste(rosters): Observable<CreatedRoster> {
    const body = JSON.stringify({ rosterId: rosters.rosterId, pasteToDate: rosters.pasteToDate, locationId: rosters.locationId });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<CreatedRoster>(this.apiBaseUrl + '/rosters/copypaste', body, httpOptions);
  }

  rollShift(rosterId: number, direction: boolean): Observable<CreatedRoster> {
    const body = JSON.stringify({ rosterId, direction });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                    'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<CreatedRoster>(this.apiBaseUrl + '/rosters/roll', body, httpOptions);
  }

  getDLIReportData(payPeriodId: number): Observable<DLICosting[]> {
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    const httpOptions = { headers: httpHeaders };

    return this._http.get<Array<DLICosting>>(this.apiBaseUrl + `/reports/dli-costing?payPeriodId=${payPeriodId}`, httpOptions);
  }

  getSiteInactivityReportData(dateFrom: string, dateTo: string,
                              entityType: string, entityIdList: number[]): Observable<SiteInactivityResponse[]> {
    const body = JSON.stringify({ dateFrom, dateTo, entityType, entityIdList });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                      'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<Array<SiteInactivityResponse>>(this.apiBaseUrl + `/reports/inactive-sites`, body, httpOptions);
  }

  getUtilisationReportData(dateFrom: string, dateTo: string,
                              entityType: string, entityIdList: number[]): Observable<UtilisationReportResponse[]> {
    const body = JSON.stringify({ dateFrom, dateTo, entityType, entityIdList });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken(),
                                      'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<Array<UtilisationReportResponse>>(this.apiBaseUrl + `/reports/utilisation`, body, httpOptions);
  }

  getShowDateTimeInputByReportType(reportType: string): boolean {
    const reportTypesToShowDateTime: string[] = ['Site Inactivity Report']; // list of reports that use DateTime Input instead of Date Input

    return (reportTypesToShowDateTime.includes(reportType));
  }

  changePassword(oldpassword: string, newpassword: string) {
    const body = JSON.stringify({ oldpassword, newpassword });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json' , 'Authorization': 'Token ' + this.getToken(),
                                      'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post(this.apiBaseUrl + '/users/change-password', body, httpOptions);
  }

  getAllRostersForEmployee(employeeId: number, dateFrom: string, dateTo: string): Observable<ExistingRoster[]> {
    const body = JSON.stringify({ employeeId, dateFrom, dateTo });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    const httpOptions = { headers: httpHeaders };
    const url = this.apiBaseUrl + '/rostersforemployee?employeeId=' + employeeId + '&dateFrom=' + dateFrom
    + '&dateTo=' + dateTo;

    return this._http.get<Array<ExistingRoster>>(url, httpOptions);
  }

  getUserReportDefaultSettingsFilter(report: string): ReportSettings {
    switch (report) {
      case 'Timesheet By Location Report':
        this.reportSettingsFilter = {
          reportName: report,
          rosterType: [{ planned: true, timekeeping: false, authorised: false }],
          reportFormat: [{ breakTime: false, hidEndTimes: false }],
          colOptions: [{ task: false, position: false }]
        };
        return this.reportSettingsFilter;
     default:
        this.reportSettingsFilter = {
          reportName: report,
          rosterType: [{ planned: true, timekeeping: false, authorised: false }],
          reportFormat: [{ breakTime: false, hidEndTimes: false }],
          colOptions: [{ task: false, position: false }]
        };
        return this.reportSettingsFilter;
    }
  }

  updateUserReportSettingsFilter(reportSet: ReportSettings): void {
    this.reportSettingsFilter = reportSet;
  }

  getEmployeeDetails(): Observable<EmployeeDetails> {
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    const httpOptions = { headers: httpHeaders };

    return this._http.get<EmployeeDetails>(this.apiBaseUrl + '/users/me/employee-details', httpOptions);
  }

  updateEmployeeDetails(details: EmployeeDetails): Observable<any> {
    const body = JSON.stringify(details);
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json' , 'Authorization': 'Token ' + this.getToken(),
                                      'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post(this.apiBaseUrl + '/users/me/employee-details', body, httpOptions);
  }

  getEmployeeLeave(): Observable<EmployeeLeave> {
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json', 'Authorization': 'Token ' + this.getToken() });
    const httpOptions = { headers: httpHeaders };

    return this._http.get<EmployeeLeave>(this.apiBaseUrl + '/users/me/leave-details', httpOptions);
  }

  sendLeaveRequest(leaveRequest: LeaveRequest): Observable<any> {
    const body = JSON.stringify(leaveRequest);
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json' , 'Authorization': 'Token ' + this.getToken(),
                                      'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    // return this._http.post<any>(this.apiBaseUrl + '/users/me/leave-request', body, httpOptions);

    // TODO
    return Observable.of('success');
  }

  acceptRejectRoster(rosterId: number, accepted: boolean): Observable<any> {
    const body = JSON.stringify({ rosterId, accepted });
    const httpHeaders = new HttpHeaders({ 'Accept': 'application/json' , 'Authorization': 'Token ' + this.getToken(),
                                      'Content-Type': 'application/json' });
    const httpOptions = { headers: httpHeaders };

    return this._http.post<any>(this.apiBaseUrl + '/rosters/me/accept-reject-roster', body, httpOptions);
  }
}
