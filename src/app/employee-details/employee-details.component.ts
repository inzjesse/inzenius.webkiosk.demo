import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { DataService } from '../data.service';

export interface EmployeeAddress {
  street: string;
  city: string;
  postCode: number;
  state: string;
  country: string;
}

export interface EmployeeDetails {
  firstName: string;
  lastName: string;
  address: EmployeeAddress;
  phoneNumber: string;
  emailAddress: string;
}

export interface FormResult {
  visible: boolean;
  success: boolean;
  message: string;
}

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {
  _dataService: DataService;
  employeeDetails: EmployeeDetails;
  employeeDetailsForm = this.formBuilder.group({
    firstName: ['', Validators.required],
    lastName: ['', Validators.required],
    address: this.formBuilder.group({
      street: ['', Validators.required],
      city: ['', Validators.required],
      postCode: ['', Validators.required],
      state: ['', Validators.required],
      country: ['', Validators.required]
    }),
    phoneNumber: [''],
    emailAddress: ['']
  });
  countries: string[] = [
    'Australia', 'New Zealand'
  ];
  submitStatus: string;
  result: FormResult = {
    visible: false,
    success: true,
    message: ''
  };

  constructor(@Inject(DataService) dataService: DataService, private formBuilder: FormBuilder) {
    this._dataService = dataService;
  }

  ngOnInit() {
    this._dataService.getEmployeeDetails().subscribe((details: EmployeeDetails) => {
      this.employeeDetails = details;
      this.employeeDetailsForm.patchValue(this.employeeDetails);
    }, error => {
      const testData: EmployeeDetails = {
        firstName: 'Allan',
        lastName: 'Alder',
        address: {
          street: '123 Fake St',
          city: 'Fakeville',
          postCode: 1234,
          state: 'VIC',
          country: 'Australia'
        },
        phoneNumber: '0311119999',
        emailAddress: 'allan@alder.com'
      };
      this.employeeDetailsForm.patchValue(testData);
    });
  }

  onSubmit(form: EmployeeDetails): void {
    this._dataService.loading = true;
    this._dataService.updateEmployeeDetails(form).subscribe((message: string) => {
      this.result.success = true;
      this.result.message = `Update successful. ${message}`;

      this._dataService.loading = false;
      this.result.visible = true;
      setTimeout(() => { this.resetFormResult(); }, 5000);
    }, error => {
      this.result.success = false;
      this.result.message = `Submission error. ${error}`;

      this._dataService.loading = false;
      this.result.visible = true;
      setTimeout(() => { this.resetFormResult(); }, 5000);
    });
  }

  resetFormResult(): void {
    this.result.visible = false;
    this.result.message = '';
  }
}
