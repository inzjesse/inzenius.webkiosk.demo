import { Component, Input, OnInit } from '@angular/core';

import { EmployeeLeave } from '../employee-leave.component';

@Component({
  selector: 'app-employee-leave-details',
  templateUrl: './employee-leave-details.component.html',
  styleUrls: ['./employee-leave-details.component.css']
})
export class EmployeeLeaveDetailsComponent implements OnInit {
  @Input() leave: EmployeeLeave;

  constructor() { }

  ngOnInit() {
  }

}
