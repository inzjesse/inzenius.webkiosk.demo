import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import * as wjcInput from 'wijmo/wijmo.input';

import { DataService } from '../../data.service';
import { EmployeeLeave } from '../employee-leave.component';
import { FormResult } from '../../employee-details/employee-details.component';

// export interface LeaveType {
//   leaveName: string;
//   leaveTypeId: number;
// }

export interface LeaveRequest {
  dateFrom: Date;
  dateTo: Date;
  leaveName: string;
  leaveTypeId: number;
  emailRecipients: string[];
  comments: string;
}

@Component({
  selector: 'app-employee-leave-request',
  templateUrl: './employee-leave-request.component.html',
  styleUrls: ['./employee-leave-request.component.css']
})
export class EmployeeLeaveRequestComponent implements OnInit {
  _dataService: DataService;
  leave: EmployeeLeave;
  leaveRequestForm: FormGroup = this.formBuilder.group({
    dateFrom: ['', Validators.required],
    dateTo: ['', Validators.required],
    leaveTypeId: ['', Validators.required],
    emailRecipients: ['', [
      Validators.required,
      // Validators.pattern('^(\s?[^\s,]+@[^\s,]+\.[^\s,]+\s?,)*(\s?[^\s,]+@[^\s,]+\.[^\s,]+)$')
      ]
    ],
    comments: ['']
  });
  leaveTypes = [
    {leaveName: 'Annual Leave', leaveTypeId: 1},
    {leaveName: 'Personal/Carer\'s Leave', leaveTypeId: 2}
  ];
  result: FormResult = {
    visible: false,
    success: true,
    message: ''
  };

  get dateFrom() { return this.leaveRequestForm.get('dateFrom'); }
  get dateTo() { return this.leaveRequestForm.get('dateTo'); }
  get leaveTypeId() { return this.leaveRequestForm.get('leaveTypeId'); }
  get emailRecipients() { return this.leaveRequestForm.get('emailRecipients'); }

  constructor(@Inject(DataService) dataService: DataService, private formBuilder: FormBuilder) {
    this._dataService = dataService;

    this._dataService.getEmployeeLeave().subscribe((leave: EmployeeLeave) => {
      this.leave = leave;
    }, error => {
      this.leave = {
        leaveType: 'Annual Leave',
        leaveBalanceHours: 300,
        leaveBalanceDays: 39.4
      };
    });
  }

  ngOnInit() {
    const today = moment(new Date()).format('YYYY-MM-DD');
    this.leaveRequestForm.patchValue(
      { dateFrom: today, dateTo: today }
    );
  }

  onSubmit(form: LeaveRequest): void {
    console.log(form);
    this._dataService.loading = true;
    this._dataService.sendLeaveRequest(form).subscribe((result) => {
      // if (result && result.message) {
      //   this.result.message = result;
      // }
      this.result.message = `✔ Request submitted successfully!`;
      this.result.success = true;

      this._dataService.loading = false;
      this.result.visible = true;
      setTimeout(() => { this.resetFormResult(); }, 5000);
    }, error => {
      this.result.message = error.message;
      this.result.success = false;

      this._dataService.loading = false;
      this.result.visible = true;
      setTimeout(() => { this.resetFormResult(); }, 5000);
    });
  }

  resetFormResult(): void {
    this.result.visible = false;
    this.result.message = '';
  }
}
