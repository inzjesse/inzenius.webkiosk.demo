import { Component, Inject, OnInit } from '@angular/core';

import { DataService } from '../data.service';

export interface EmployeeLeave {
  leaveType: string;
  leaveBalanceHours: number;
  leaveBalanceDays: number;
}

@Component({
  selector: 'app-employee-leave',
  templateUrl: './employee-leave.component.html',
  styleUrls: ['./employee-leave.component.css']
})
export class EmployeeLeaveComponent implements OnInit {
  _dataService: DataService;
  leave: EmployeeLeave;

  constructor(@Inject(DataService) dataService: DataService) {
    this._dataService = dataService;

    // this._dataService.getEmployeeLeave().subscribe((leave: EmployeeLeave) => {
    //   this.leave = leave;
    // });
  }

  ngOnInit() { }

}
