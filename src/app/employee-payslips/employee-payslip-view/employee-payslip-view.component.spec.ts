import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePayslipViewComponent } from './employee-payslip-view.component';

describe('EmployeePayslipViewComponent', () => {
  let component: EmployeePayslipViewComponent;
  let fixture: ComponentFixture<EmployeePayslipViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePayslipViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePayslipViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
