import { Component, OnInit, Input } from '@angular/core';
import { Payslip } from '../employee-payslips.component';

@Component({
  selector: 'app-employee-payslip-view',
  templateUrl: './employee-payslip-view.component.html',
  styleUrls: ['./employee-payslip-view.component.css']
})
export class EmployeePayslipViewComponent implements OnInit {

  @Input() payslip: Payslip;

  constructor() { }

  ngOnInit() {
  }

}
