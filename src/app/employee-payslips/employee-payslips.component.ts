import { Component, OnInit, EventEmitter } from '@angular/core';
import * as wjcInput from 'wijmo/wijmo.input';

export enum PayRunType {
  Regular = 1,
  Manual = 2,
  Rebank = 3,
  ETP = 4,
  Final = 5
}

export interface Payslip {
  employeeId: number;
  payslipId: number;
  payRunTypeId: PayRunType;
  payRunName: string;
  payPeriodStartDate: Date;
  payPeriodEndDate: Date;
  commitDate: Date;
}

@Component({
  selector: 'app-employee-payslips',
  templateUrl: './employee-payslips.component.html',
  styleUrls: ['./employee-payslips.component.css']
})
export class EmployeePayslipsComponent implements OnInit {

  payslipViewModal: wjcInput.Popup;
  showPayslip: EventEmitter<Payslip> = new EventEmitter<Payslip>();

  payslips: Payslip[] = [
    { employeeId: 1, payslipId: 1, payRunTypeId: 1, payRunName: 'Test', payPeriodStartDate: new Date(), payPeriodEndDate: new Date(), commitDate: new Date() },
    { employeeId: 1, payslipId: 2, payRunTypeId: 1, payRunName: 'Test', payPeriodStartDate: new Date(), payPeriodEndDate: new Date(), commitDate: new Date() },
    { employeeId: 1, payslipId: 3, payRunTypeId: 1, payRunName: 'Test', payPeriodStartDate: new Date(), payPeriodEndDate: new Date(), commitDate: new Date() },
    { employeeId: 1, payslipId: 4, payRunTypeId: 1, payRunName: 'Test', payPeriodStartDate: new Date(), payPeriodEndDate: new Date(), commitDate: new Date() },
    { employeeId: 1, payslipId: 5, payRunTypeId: 1, payRunName: 'Test', payPeriodStartDate: new Date(), payPeriodEndDate: new Date(), commitDate: new Date()}
  ];

  constructor() {
    // testin'
    this.payslips = [...this.payslips, ...this.payslips, ...this.payslips, ...this.payslips,
                    ...this.payslips, ...this.payslips, ...this.payslips, ...this.payslips ];
  }

  ngOnInit() {
  }

  onClickViewPayslip(payslip: Payslip): void {
    this.payslipViewModal.show();
    this.showPayslip.emit(payslip);
  }

  onClickDownloadPayslip(payslip: Payslip): void {
    const downloadURL = 'assets/downloads/payslip-allan.alder.pdf';
    window.open(downloadURL);
  }
}
