export interface CheckboxEvent extends Event {
  srcElement: CheckboxElement;
}

export interface CheckboxElement extends Element {
  checked: boolean;
}
