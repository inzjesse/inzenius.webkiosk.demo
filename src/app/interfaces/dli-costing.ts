export interface DLICosting {
    plannedCost: number;
    plannedTime: number;
    plannedHours: number;
    actualCost: number;
    actualTime: number;
    actualHours: number;
    hoursVariance: number;
    costVariance: number;
    payPeriodStartDate: Date;
    payPeriodEndDate: Date;
    payRunScheduleName: string;
    costCentreName: string;
    costCentreId: number;
    locationName: string;
    locationId: number;
    locationType: string; // Standard, Special, Periodical, or Subcontractor
    award: string;
    costClassification: string; // 'SickLeave', 'Overtime', or null
    managerEmployeeId: number;
    managerEmployeeNumber: number;
    managerFirstName: string;
    managerLastName: string;
    managerFullName: string;
    subcontractorCost: number;
    periodicCost: number;
    sickLeaveCost: number;
    overtimeCost: number;
    sickLeaveHours: number;
    overtimeHours: number;
}

export interface DLISummary {
    name: string;
    plannedHours: number;
    actualHours: number;
    hoursVariance: number;
    plannedCost: number;
    subcontractorCost: number;
    periodicCost: number;
    actualCost: number;
    costVariance: number;
    sickLeaveHours: number;
    sickLeaveCost: number;
    overtimeHours: number;
    overtimeCost: number;
}
