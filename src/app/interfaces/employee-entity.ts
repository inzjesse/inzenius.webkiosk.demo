export interface EmployeeEntity {
    entityBold: number;
    entityDisabled: number;
    entityId: number;
    entityName: string;
    entityParentId: string;
}
