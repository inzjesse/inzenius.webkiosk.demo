export interface EmployeeRosterByPeriod {
    shiftId: number;
    shiftStartDate: Date;
    shiftCompositionCode: string;
    strSkillset: string;
    startTime: Date;
    finishTime: Date;
    strLocation: string;
    strEvent: string;
    strPosition: string;
}
