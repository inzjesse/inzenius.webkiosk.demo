export interface ExistingRoster {
    allowanceDetails: string;
    allowanceIds: number[];
    assetIds: number[];
    assetName: string;
    breakEnd: string; // obsolete, use breakEndFull instead
    breakEndFull: Date;
    breakLength: number;
    breakPaid: boolean;
    breakStart: string; // obsolete, use breakStartFull instead
    breakStartFull: Date;
    costCentreId: number;
    costCentreName: string;
    deductionDetails: string;
    deductionIds: number[];
    employeeId: number;
    employeeName: string;
    endTime: string; // obsolete, use endTimeFull instead
    endTimeFull: Date;
    engagementLength: string; // obsolete, use engagementLengthFull instead
    engagementLengthFull: number;
    eventId: number;
    eventName: string;
    gradeId: number;
    gradeName: string;
    isSigned: boolean;
    locationId: number;
    locationName: string;
    positionId: number;
    positionName: string;
    rosterAcceptanceTypeDisplay: string; // roster acceptance type icon content based on type id
    rosterAcceptanceTypeId: number;
    rosterId: number;
    rosterNotes: string;
    rosterTypeId: number;
    ruleIds: number[];
    ruleName: string;
    shiftCompositionCode: string;
    shiftCompositionCodeDisplay: string; // code content to display in roster grid (empty unless single roster type)
    shiftEntityTypeId: number;
    shiftId: number;
    startDate: string; // obsolete, use startDateFull instead
    startDateFull: Date;
    startTime: string; // obsolete, use startTimeFull instead
    startTimeFull: Date;
    taskIds: number[];
    taskName: string;
}
