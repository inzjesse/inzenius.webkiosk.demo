export interface Login {
    token: string;
    claims: string[];
    username: string;
    employeeDisplayName: string;
}
