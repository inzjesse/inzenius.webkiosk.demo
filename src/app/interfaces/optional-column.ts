export interface OptionalColumn {
    columnName: string;
    columnBinding: string;
}

export const TaskColumn: OptionalColumn = {
    columnName: 'Task',
    columnBinding: 'taskName'
  };
export const PositionColumn: OptionalColumn = {
    columnName: 'Position',
    columnBinding: 'positionName'
};
