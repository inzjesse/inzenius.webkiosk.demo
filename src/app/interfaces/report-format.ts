export interface ReportFormat {
    breakTime: boolean;
    hidEndTimes: boolean;
}
