import { RosterType } from './roster-type';
import { ReportFormat } from './report-format';

export interface ReportSettings {
    reportName: string;
    rosterType: RosterType[];
    reportFormat: ReportFormat[];
    colOptions: ReportColumns[];
}

interface ReportColumns {
    task: boolean;
    position: boolean;
}

