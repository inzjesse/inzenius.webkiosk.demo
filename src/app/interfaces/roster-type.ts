export interface RosterType {
    planned: boolean;
    timekeeping: boolean;
    authorised: boolean;
}
