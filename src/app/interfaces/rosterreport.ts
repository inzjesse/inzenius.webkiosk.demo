export interface RosterReport {
    manager: string;
    date: Date;
    siteName: string;
    siteID: number;
    contractor: string;
    employeeName: string;
    pin: string;
    daysofCleaning: string;
    aMPMClean: string;
    loginTime: Date;
    logoutTime: Date;
    actualHours: number;
    budgetedHours: number;
    variance: number;
    comments: string;
}
