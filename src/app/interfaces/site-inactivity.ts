export interface SiteInactivityResponse {
    entityId: number;
    entityName: string;
}
