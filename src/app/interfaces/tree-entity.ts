export interface TreeEntity {
    entityBold: number;
    entityDisabled: number;
    entityId: string;
    entityLevel: number;
    entityName: string;
    entityParentId: string;
    entitySequence: number;
    entityType: number;
    children?: TreeEntity[];
    visible?: boolean;
    selected?: boolean;
}
