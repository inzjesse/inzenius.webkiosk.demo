export interface UtilisationReportResponse {
    entityName: string; // Location or Cost Centre
    entityId: number;
    rosterId: number;
    rosterDate: Date;
    employeeName: string;
    employeeId: number;
    siteId: number; // unused for now
    gradeName: string;
    gradeId: number;
    budgetedHours: number;
    actualHours: number;
    variance: number;
}
