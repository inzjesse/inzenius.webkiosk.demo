import { Component, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import { DataService } from '../data.service';
import { Login } from '../interfaces/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy {

  loading = false;
  ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private _router: Router, private _dataService: DataService) { }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  login(form: NgForm) {
    this.loading = true;
    const username = form.value.login;
    const password = form.value.password;
    const connectionName = form.value.name;
    const connectionUrl = form.value.url;

    // localStorage.setItem('currentConnection', this.selectDb === false ? connectionUrl : this.selectedConnection);
    this._dataService.apiBaseUrl = localStorage.getItem('currentConnection');

    this._dataService.login(username, password)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((login: Login) => {
        this.loading = false;
        // if (this.selectDb === false) { this.saveConnection(connectionName, connectionUrl); }

        this._dataService.saveToken(login.token);
        this._dataService.refreshUserReportTypes(); // load user report permissions (used in routing)
        this._router.navigate(['rostering']);
        this._dataService.loggedIn.emit(login);
      }, error => {
        this.loading = false;
        error = error.error;
        if (error.description && error.errors.length) {
          alert(`${error.description}. ${error.errors}.`);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
      });
  }

  // saveConnection(connectionName: string, connectionUrl: string) {
  //   const newConnection = {name: connectionName, url: connectionUrl};
  //   const newConnectionList: {name: string, url: string}[] = this.connectionList || [];

  //   newConnectionList.push(newConnection);
  //   localStorage.setItem('connectionList', JSON.stringify(newConnectionList));
  // }

}
