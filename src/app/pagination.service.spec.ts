import { TestBed, inject } from '@angular/core/testing';

import { PaginationService } from './pagination.service';
import { DataServiceStub } from '../testing/helpers';
import { DataService } from './data.service';

describe('PaginationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ PaginationService,
        { provide: DataService, useClass: DataServiceStub },
      ]
    });
  });

  it('should be created', inject([PaginationService], (paginationService) => {
    expect(paginationService).toBeTruthy();
  }));
});
