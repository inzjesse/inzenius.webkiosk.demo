import { Injectable } from '@angular/core';
import * as _ from 'underscore';

import { DataService } from './data.service';
import { PaginationProperties } from './interfaces/pagination-properties';

@Injectable()
export class PaginationService {
    constructor(private _dataService: DataService) { }

    getPager(totalItems: number, currentPage: number = 1, pageSize: number = 1): PaginationProperties {
        // calculate total pages
        const totalPages = Math.ceil(totalItems / pageSize);
        let startPage: number, endPage: number;
        // logic to display page Format link as << 1 2 3 4 5 >>
        const maxPageLinksDisplayed = 5;
        if (totalPages <= maxPageLinksDisplayed) {
            startPage = 1;
            endPage = totalPages;
        } else {
            if (currentPage <= Math.ceil(maxPageLinksDisplayed / 2)) {
                startPage = 1;
                endPage = maxPageLinksDisplayed;
            } else if (currentPage + 1 >= totalPages) {
                startPage = totalPages - (maxPageLinksDisplayed - 1);
                endPage = totalPages;
            } else {
                startPage = currentPage - 2;
                endPage = currentPage + 2;
            }
        }
        // end page format link

        // calculate start and end item indexes
        const startIndex = (currentPage - 1) * pageSize;
        const endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        const pages = _.range(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }
}
