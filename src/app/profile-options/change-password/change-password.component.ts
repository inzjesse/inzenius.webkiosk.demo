import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import { DataService } from '../../data.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnDestroy {

  ngUnsubscribe: Subject<void> = new Subject<void>();
  passwordErr: string;
  loading: boolean;
  userName: string;
  currentpassword: string;
  newpassword: string;
  confirmpassword: string;
  changeSuccess: boolean;

  constructor(private _router: Router, private _dataService: DataService) {
    this.setDefaults();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ChangePwd(form: NgForm) {

    this.loading = true;
    this.userName = this._dataService.getUser();
    this.currentpassword = form.value.currentpassword;
    this.newpassword = form.value.newpassword;
    this.confirmpassword = form.value.confirmpassword;

    if (this.passwordErr == null && (this.currentpassword.length > 0 && this.newpassword.length > 0 && this.confirmpassword.length > 0)) {
      this._dataService.changePassword(this.currentpassword, this.newpassword)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(res => {
          this.changeSuccess = true;
          this.passwordErr = 'Password updated successfully!';
          this.currentpassword = '';
          this.newpassword = '';
          this.confirmpassword = '';
          this.loading = false;

        }, error => {
          this.loading = false;
          this.changeSuccess = false;
          error = error.error;
          if (error.description) { this.passwordErr = error.description; } else {
            alert('Server not available, please contact support@inzenius.com');
          }
      });
    }
  }

  focusOutFunction() {
    if (this.newpassword === this.currentpassword && (this.newpassword.length > 0 && this.currentpassword.length > 0)) {
      this.passwordErr = 'New Password is entered same as Current Password!';
    } else if (this.newpassword !== this.confirmpassword && (this.newpassword.length > 0 && this.confirmpassword.length > 0)) {
      this.passwordErr = 'New Password and Confirm Password fields do not match!';
    } else {
      this.passwordErr = null;
    }
  }

  cancelChangePwd() {
    this.setDefaults();
  }

  setDefaults() {
    this.passwordErr = '';
    this.newpassword = '';
    this.confirmpassword = '';
    this.currentpassword = '';
    this.changeSuccess = false;
  }
}
