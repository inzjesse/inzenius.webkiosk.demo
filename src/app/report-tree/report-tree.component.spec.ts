import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms'; // for (ngModel)
import { WjInputModule } from 'wijmo/wijmo.angular2.input';
import { WjGridModule } from 'wijmo/wijmo.angular2.grid';
import { Observable } from 'rxjs/Observable';

import { DataServiceStub } from '../../testing/helpers';
import { DataService } from '../data.service';
import { ReportTreeComponent } from './report-tree.component';
import { TreeViewComponent } from '../tree/tree-view/tree-view.component';

describe('ReportTreeComponent', () => {
  let component: ReportTreeComponent;
  let fixture: ComponentFixture<ReportTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, WjInputModule ],
      declarations: [ ReportTreeComponent, TreeViewComponent ],
      providers: [
        { provide: DataService, useClass: DataServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have report permission', () => {
    // report types already loaded on component creation

    const reportTypes = [{ reportType: 'test', reportName: 'Test Report' }];

    // get data service instance from component instance
    const dataService: DataServiceStub = fixture.debugElement.injector.get(DataService);

    expect(dataService.userReportTypes).toEqual(reportTypes, 'data service user report types');
    expect(component.reportTypeList.items).toEqual(reportTypes, 'component report type list');
  });
});
