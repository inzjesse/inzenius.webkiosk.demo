import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcInput from 'wijmo/wijmo.input';

import { TreeComponent } from '../tree/tree.component';
import { DataService } from '../data.service';
import { EntityType } from '../interfaces/entity-type';
import { TreeEntity } from '../interfaces/tree-entity';
import { ReportType } from '../interfaces/report-type';

@Component({
  selector: 'app-report-tree',
  templateUrl: './report-tree.component.html',
  styleUrls: ['./report-tree.component.css']
})
export class ReportTreeComponent extends TreeComponent implements OnDestroy {

  reportTypeList: wjcCore.CollectionView;
  buttonType = 'checkbox';
  previousSelectedNode: string;
  showDateTimeInput = false; // flag to determine whether we show DateInput or DateTimeInput
  ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(_dataService: DataService) {
    super(_dataService);
    this.fillReportTypeList();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  fillReportTypeList(): void {
    this._dataService.getReportTypes().pipe(takeUntil(this.ngUnsubscribe)).subscribe((reports) => {
      // user does not have permission to view any reports so insert a filler message into drop down
      if (!reports.length) {
        reports = [{ reportType: '', reportName: 'No Reports Available' }];
      }
      this._dataService.userReportTypes = reports;
      this.reportTypeList = new wjcCore.CollectionView(this._dataService.userReportTypes);
      this._dataService.selectedReport = reports[0].reportName;
    }, errors => {
      // test reports
      const testReports: ReportType[] = [
        // { reportName: 'Attendance Report', reportType: 'attendance' },
        { reportName: 'Timesheet By Employee Report', reportType: 'timesheetbyemployeereport' }
      ];
      this._dataService.userReportTypes = testReports;
      this.reportTypeList = new wjcCore.CollectionView(this._dataService.userReportTypes);
    });
  }

  showReport(reportType: string, entityType: string, reportName: string): void {
    // reset list first
    this.selectedEntities = '';
    const entitiesFilter = this.getSelectedEntities(this.myTree).slice(0, - 1);
    this._dataService.selectedReport = reportName;
    this._dataService.enableToolbar = false;
    this._dataService.refreshViewer.emit();
    const eventData = {
      reportType: reportType,
      entitiesFilter: entitiesFilter,
      entityType: entityType,
      dateFrom: this.formatDateTime(this.dateFrom),
      dateTo: this.formatDateTime(this.dateTo)
    };
    this._dataService.showReport.emit(eventData);
  }

  // modify the entity type drop down list for DLI Report
  // the entity type is not loaded from the API for this report so we insert a single entry manually
  // the entity tree is still loaded via the API
  checkEntityTypes(reportType: string, comboBox: wjcInput.ComboBox): void {

    if (reportType === 'DLI Report') {
      // insert Pay Period into Entity Type drop down
      const dliEntityTypes: EntityType[] = [{ entityName: 'Pay Period' }];
      this.entityTypeList.sourceCollection = dliEntityTypes;

      // swap button input type since we can only select one pay period
      this.buttonType = 'radio';

      // manually update combo box text to retrieve pay period entity tree
      comboBox.text = dliEntityTypes[0].entityName;
      super.getTreeEntities(comboBox.text);
    } else {
      this.buttonType = 'checkbox'; // reset to default (checkbox)
      super.getEntityTypes(comboBox);
    }

    this.checkInputType(reportType);
    this._dataService.selectedReport = reportType;
  }

  getSelectedEntitiesForReport(tree: TreeEntity[]) {

    if (this.buttonType === 'radio') {
      this.UnselectPreviousSelection(tree);
    }

    for (const node of tree) {
      if (node.children.length) {
        this.getSelectedEntitiesForReport(node.children);
      }
      if (node.selected) {
        this.selectedEntities += node.entityId + ',';
        if (this.buttonType === 'radio') {
          this.previousSelectedNode = node.entityId;
        }
      }
    }

    return this.selectedEntities;
  }

  UnselectPreviousSelection(treeNodes: TreeEntity[]) {
    for (const node of treeNodes) {
      if (node.children.length) {
        this.UnselectPreviousSelection(node.children);
      }
      if (node.entityId === this.previousSelectedNode) {
        node.selected = false;
      }
    }
  }

  checkInputType(reportType: string): void {
    this.showDateTimeInput = this._dataService.getShowDateTimeInputByReportType(reportType); // (reportType === 'Site Inactivity Report');

    // reset HH:mm vale of date inputs to 00:00 when changing report types
    this.dateFrom.setHours(0, 0, 0);
    this.dateTo.setHours(0, 0, 0);
  }

  onDateValueChange(dateLabel: string, dateValue: Date, selectedEntityType: string): void {
    if (dateLabel === 'dateFrom' && this.formatDateTime(dateValue) !== this.formatDateTime(this.dateFrom)) {

      const oldDateFrom = this.dateFrom;

      this.dateFrom = dateValue;

      // only reload entites when the DATE changes (not the TIME)
      if (this.formatDate(dateValue) !== this.formatDate(oldDateFrom)) {
        this.getTreeEntities(selectedEntityType);
      }

      localStorage.setItem('rosteringDateFrom', this.dateFrom.toISOString());

      // update filter data when user changes date
      // we need an accurate filter data for pasting rosters
      if (this._dataService.filterData) { this._dataService.filterData.dateFrom = this.formatDate(this.dateFrom); }
    }

    if (dateLabel === 'dateTo' && this.formatDateTime(dateValue) !== this.formatDateTime(this.dateTo)) {

      const oldDateTo = this.dateTo;

      this.dateTo = dateValue;

      // only reload entites when the DATE changes (not the TIME)
      if (this.formatDate(dateValue) !== this.formatDate(oldDateTo)) {
        this.getTreeEntities(selectedEntityType);
      }

      localStorage.setItem('rosteringDateTo', this.dateTo.toISOString());

      // update filter data when user changes date
      // we need an accurate filter data for pasting rosters
      if (this._dataService.filterData) { this._dataService.filterData.dateTo = this.formatDate(this.dateTo); }
    }
  }

  formatDateTime(dateObject: Date): string {
    const year  = dateObject.getFullYear();
    let month: any = dateObject.getMonth() + 1;
    let day: any   = dateObject.getDate();
    let hh: any = dateObject.getHours();
    let mm: any = dateObject.getMinutes();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    if (hh < 10) {
      hh = '0' + hh;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }

    return `${year}-${month}-${day}T${hh}:${mm}`;
  }
}
