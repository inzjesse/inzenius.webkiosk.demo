/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter, NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ReportComponent } from './report.component';
import { DataService } from '../data.service';
import { WjGridModule } from 'wijmo/wijmo.angular2.grid';
import { WjInputModule } from 'wijmo/wijmo.angular2.input';

class FakeDataService {
  showReport = new EventEmitter<any>();
  onToggleFilters = new EventEmitter<any>();

  getEntityTypes() {
    return Observable.of({entityName: 'Cost Centre'});
  }
  canViewReport(type: string): boolean {
    if (type === 'raw') { return true; } // access for test

    return false; // no access
  }
}

// jasmine spy for calls to router.navigate
const router = {
  navigate: jasmine.createSpy('navigate')
};

describe('ReportComponent', () => {
  let component: ReportComponent;
  let fixture: ComponentFixture<ReportComponent>;

  beforeEach(async(() => {
    const dataServiceStub = {
      showReport: new EventEmitter<any>(),
      onToggleFilters: new EventEmitter<any>()
    };

    TestBed.configureTestingModule({
      imports: [ WjGridModule, WjInputModule ],
      declarations: [ ReportComponent ],
      providers: [
        { provide: Router, useValue: router },
        { provide: DataService, useClass: FakeDataService },
      ],
      schemas: [ NO_ERRORS_SCHEMA ] // ignore unknown elements for child components
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should toggle showTree from false to true', () => {
    component.showTree = false;
    component.toggleTree();
    fixture.detectChanges();
    expect(component.showTree).toBeTruthy();
  });

  it('should toggle showTree from true to false', () => {
    component.showTree = true;
    component.toggleTree();
    fixture.detectChanges();
    expect(component.showTree).toBeFalsy();
  });

  it('should subscribe to showReport and call router.navigate', () => {
    const data = { // test data to emit
      reportType: 'raw',
      entitiesFilter: '1',
      entityType: 'Cost Centre',
      dateFrom: '2016/11/11',
      dateTo: '2016/11/12'
    };
    component._dataService.showReport.emit(data);

    const navUrl = 'reports/raw';
    const navData = {
      from: '2016/11/11',
      to: '2016/11/12',
      et: 'Cost Centre',
      ef: '1'
    };
    expect(router.navigate).toHaveBeenCalledWith([navUrl, navData]);
    router.navigate.calls.reset();
  });

  it('should redirect to /reports/ when report type parameter is invalid', () => {
    const data = {
      reportType: 'not-a-real-report',
      entitiesFilter: '1',
      entityType: 'Cost Centre',
      dateFrom: '2016/11/11',
      dateTo: '2016/11/12'
    };
    component._dataService.showReport.emit(data);

    expect(router.navigate).toHaveBeenCalledWith(['reports']);
    router.navigate.calls.reset();
  });
});
