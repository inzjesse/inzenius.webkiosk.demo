import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcInput from 'wijmo/wijmo.input';

import { ReportRoutes } from './report.routing';
import { DataService } from '../data.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements AfterViewInit {
  @ViewChild('report') report: ElementRef;
  _dataService: DataService;
  showTree = true;

  constructor(_dataService: DataService, router: Router) {
    this._dataService = _dataService;

    this._dataService.showReport.subscribe((res: any) => {
        const params = res;
        const reportType = params.reportType;
        const canView = this._dataService.canViewReport(reportType);

        // verify that user has permission to view this report type
        if (canView) {
          const from = params.dateFrom;
          const to = params.dateTo;
          const ef = params.entitiesFilter;
          const et = params.entityType;
          router.navigate(['reports/' + reportType, { from: from, to: to, et: et, ef: ef }]);
        } else { // user does not have permission so redirect to initial report page
          router.navigate(['reports']);
        }
      },
      error => {
        alert('Error while trying to show reports');
      });
  }

  ngAfterViewInit() {
    // refresh grid on class toggle
    const observer = new MutationObserver(mutations => {
      mutations.forEach(mutation => {
        if (this._dataService.reportGrid) { this._dataService.reportGrid.refresh(); }
        if (this._dataService.reportSummaryGrid) { this._dataService.reportSummaryGrid.refresh(); }
      });
    });

    const config = { attributes: true, childList: true, characterData: true };
    observer.observe(this.report.nativeElement, config);
  }

  toggleTree(): void {
    this.showTree = !this.showTree;
  }
}
