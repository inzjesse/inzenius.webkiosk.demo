import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from '../auth-guard.service';
import { ReportComponent } from './report.component';
import { LoctimesheetReportComponent } from '../reports/loctimesheet-report/loctimesheet-report.component';
import { EmptimesheetReportComponent } from '../reports/emptimesheet-report/emptimesheet-report.component';


export const ReportRoutes: Routes = [
  {
    path: 'reports',
    component: ReportComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: 'timesheetbylocationreport',
        component: LoctimesheetReportComponent
      },
      {
        path: 'timesheetbyemployeereport',
        component: EmptimesheetReportComponent
      }
    ]
  }
];
