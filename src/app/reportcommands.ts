import { Inject } from '@angular/core';
import { DataService } from './data.service';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';

export class Reportcommands {
  _dataService: DataService;

  constructor(@Inject(DataService) _dataService: DataService) {
    this._dataService = _dataService;
  }

  filterReportCommand = {
    executeCommand: () => {
      this._dataService.showFilterReportPopup.emit();
      this._dataService.filterReportPopup.show();
    }
  };
}
