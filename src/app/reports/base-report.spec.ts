import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { WjInputModule } from 'wijmo/wijmo.angular2.input';
import { WjGridModule } from 'wijmo/wijmo.angular2.grid';

import { DataService } from '../data.service';
import { ActivatedRouteStub, DataServiceStub, RouterStub } from '../../testing/helpers';
import { BaseReportComponent } from './base-report';

describe('Base Report', () => {
  let component: BaseReportComponent;
  let fixture: ComponentFixture<BaseReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ WjGridModule, WjInputModule ],
      declarations: [ BaseReportComponent ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: DataService, useClass: DataServiceStub },
        { provide: Router, useClass: RouterStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
