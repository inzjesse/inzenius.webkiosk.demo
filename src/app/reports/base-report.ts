import { Component, Inject, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DatePipe } from '@angular/common';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcInput from 'wijmo/wijmo.input';
import * as wjcGrid from 'wijmo/wijmo.grid';
import * as wjcXlsx from 'wijmo/wijmo.xlsx';
import * as wjcGridXlsx from 'wijmo/wijmo.grid.xlsx';
import * as moment from 'moment';

import { DataService } from '../data.service';

@Component({ // use component decorator to fix unit testing issues (inheritance)
  template: `` // empty template intentionally
})
export class BaseReportComponent implements OnDestroy {
  _dataService: DataService;
  route: ActivatedRoute;
  router: Router;
  params: any;
  datePipe: DatePipe;
  dateFrom: any;
  dateTo: any;
  reportData: wjcCore.CollectionView;
  reportSummaryData: wjcCore.CollectionView;
  grid: wjcGrid.FlexGrid;
  summaryGrid: wjcGrid.FlexGrid;
  reportLoading = false;
  userOptions: {} = null;
  userSettingTimeformat = 'HH:mm';


  constructor(@Inject(DataService) _dataService: DataService, route: ActivatedRoute, router: Router) {
    this._dataService = _dataService;
    this.getUserOptions();
    this.route = route;
    this.router = router;
  }

  ngOnDestroy() {
    if (this.params) { this.params.unsubscribe(); }
  }

  exportExcel(grid: wjcGrid.FlexGrid, includeColumnHeader: boolean, fileName: any, includeCellStyles?: boolean): wjcXlsx.Workbook {
    const workbook = wjcGridXlsx.FlexGridXlsxConverter.save(grid, {
                                                      includeColumnHeaders: includeColumnHeader,
                                                      includeCellStyles: includeCellStyles
                                                    }, fileName);
    return workbook;
  }

  workbookRowBuilder(val: any): wjcXlsx.WorkbookRow {
    const row = new wjcXlsx.WorkbookRow();
    row.cells.push(this.workbookCellBuilder(val));

    return row;
  }

  // create a workbook cell containing the given value
  workbookCellBuilder(val: any): wjcXlsx.WorkbookCell {
    const cell = new wjcXlsx.WorkbookCell();
    cell.value = val;
    return cell;
  }

  initGrid(grid: wjcGrid.FlexGrid, isSummary?: boolean): void {
    if (isSummary === true) {
      this.summaryGrid = grid;
      this._dataService.reportSummaryGrid = grid;
    } else {
      this.grid = grid;
      this._dataService.reportGrid = grid;
    }
  }

  // apply sorting to report grid based on properties passed in the parameter array
  sortGrid(properties: string[]): void {
    const cv = this.reportData;

    for (const property of properties) {
      const sort = new wjcCore.SortDescription(property, true); // ascending by default
      cv.sortDescriptions.push(sort);
    }
  }

  // apply gropuing to report grid based on properties passed in the parameter array
  groupGrid(properties: string[]): void {
    const cv = this.reportData;

    for (const property of properties) {
      const sort = new wjcCore.PropertyGroupDescription(property); // ascending by default
      cv.groupDescriptions.push(sort);
    }
  }


  getUserOptions() {
    this._dataService.getUserOptions()
      .subscribe(userOptions => {
        this.userOptions = userOptions;
        this.getUserSettingsTimeFormat();
      }, error => {
        error = error.json();
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
      });
  }

  getUserSettingsTimeFormat() {

    switch (this.userOptions['timeFormat']) {
      case 11:
        // twentyFourHourTime
        this.userSettingTimeformat = 'HH:mm';
        break;
      case 9:
        // twelveHourTimeSingleLowercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm t';
        break;
      case 7:
        // twelveHourTimeDoubleLowercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm tt';
        break;
      case 8:
        // twelveHourTimeSingleUppercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm t';
        break;
      case 6:
        // twelveHourTimeDoubleUppercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm tt';
        break;
      default:
        // 24 hour format
        this.userSettingTimeformat = 'HH:mm';
    }
  }


  // Render a flex grid as a basic html table
  renderTable(flex: wjcGrid.FlexGrid, tableFooter?: string): string {

    // start table
    let tbl = '<table>';

    // headers
    if (flex.headersVisibility && wjcGrid.HeadersVisibility.Column) {
        tbl += '<thead>';
        for (let r = 0; r < flex.columnHeaders.rows.length; r++) {
            tbl += this.renderRow(flex.columnHeaders, r);
        }
        tbl += '</thead>';
    }

    // body
    tbl += '<tbody>';
    for (let r = 0; r < flex.rows.length; r++) {
        tbl += this.renderRow(flex.cells, r);
    }
    tbl += '</tbody>';

    // footer
    if (tableFooter) {
      tbl += tableFooter;
    }

    // done
    tbl += '</table>';
    return tbl;
  }

  // Render a flex grid row as html table row
  renderRow(panel: wjcGrid.GridPanel, r: number): string {
    let tr = '';
    const row: wjcGrid.Row = panel.rows[r];

    if (row.renderSize > 0) {
      tr += '<tr>';
      for (let c = 0; c < panel.columns.length; c++) {
        const col: wjcGrid.Column = panel.columns[c];
        if (col.renderSize > 0) {

          // get cell style, content
          const style = 'width:' + col.renderSize + 'px;text-align:' + col.getAlignment() + ';font-size:8pt;border-bottom:1px solid #ddd;';
          let content = panel.getCellData(r, c, true);

          if (!row.isContentHtml && !col.isContentHtml) {
              content = wjcCore.escapeHtml(content);
          }

          // add cell to row
          if (panel.cellType === wjcGrid.CellType.ColumnHeader) {
              tr += '<th style="' + style + '">' + content + '</th>';
          } else {

              // show boolean values as checkboxes
              const raw = panel.getCellData(r, c, false);
              if (raw === true) {
                  content = '&#9745;';
              } else if (raw === false) {
                  content = '&#9744;';
              }

              tr += '<td style="' + style + '">' + content + '</td>';
          }
        }
      }
      tr += '</tr>';
    }
    return tr;
  }

  print(reportHeader: string, grid: wjcGrid.FlexGrid): void {
    const printDoc = new wjcCore.PrintDocument();

    printDoc.append(`<p style='text-align:center'>${reportHeader}</p>`);

    printDoc.append(this.renderTable(grid));

    printDoc.print();
  }

  /**
   * Helper function to resize the column widths within a Wijmo Workbook to fit their contents
   *
   * @param {wjcXlsx.Workbook} workbook
   * @returns {wjcXlsx.Workbook}
   * @memberof BaseReportComponent
   */
  resizeWorkbookColumnsToContentSize(workbook: wjcXlsx.Workbook): wjcXlsx.Workbook {
    for (const sheet of workbook.sheets) {
      const EXCEL_DEFAULT_CHARACTER_WIDTH = 10;
      const columnContentMaxLengths: number[] = [];

      for (const row of sheet.rows) {
        for (let i = 0; i < row.cells.length; i++) {
          const cellValue = row.cells[i].value;
          let valueLength: number = EXCEL_DEFAULT_CHARACTER_WIDTH;

          // determine cell contents character length
          if (cellValue) {
            if (cellValue instanceof Date) { // format dates before checking character length
              valueLength = moment(cellValue).format('YYYY-MM-DD').length;
            } else {
              valueLength = cellValue.length;
            }
          }

          // save column content length if larger than existing value
          if (!columnContentMaxLengths[i]) {
            columnContentMaxLengths.push(valueLength || 0);
          } else if (columnContentMaxLengths[i] < valueLength) {
            columnContentMaxLengths[i] = valueLength;
          }
        }
      }
      // update column widths to content length
      for (let i = 0; i < sheet.columns.length; i++) {
        sheet.columns[i].width = `${columnContentMaxLengths[i]}ch`;
      }
    }
    return workbook;
  }
}
