import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WjInputModule } from 'wijmo/wijmo.angular2.input';
import { WjGridModule } from 'wijmo/wijmo.angular2.grid';
import { ActivatedRoute, Router } from '@angular/router';

import { ActivatedRouteStub, DataServiceStub, PaginationServiceStub, RouterStub } from '../../../testing/helpers';
import { DataService } from '../../data.service';
import { EmptimesheetReportComponent } from './emptimesheet-report.component';
import { PaginationService } from '../../pagination.service';

describe('EmptimesheetReportComponent', () => {
  let component: EmptimesheetReportComponent;
  let fixture: ComponentFixture<EmptimesheetReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ WjInputModule, WjGridModule ],
      declarations: [ EmptimesheetReportComponent ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: DataService, useClass: DataServiceStub },
        { provide: Router, useClass: RouterStub },
        { provide: PaginationService, useClass: PaginationServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptimesheetReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
