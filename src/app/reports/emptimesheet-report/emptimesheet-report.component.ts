import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DatePipe } from '@angular/common';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcInput from 'wijmo/wijmo.input';
import * as wjcGrid from 'wijmo/wijmo.grid';
import * as moment from 'moment';
import * as wjcXlsx from 'wijmo/wijmo.xlsx';
import * as wjcGridXlsx from 'wijmo/wijmo.grid.xlsx';

import { DataService } from '../../data.service';
import { PaginationService } from '../../pagination.service';
import { BaseReportComponent } from '../base-report';
import { PaginationProperties } from '../../interfaces/pagination-properties';

@Component({
  selector: 'app-emptimesheet-report',
  templateUrl: './emptimesheet-report.component.html',
  styleUrls: ['./emptimesheet-report.component.scss']
})
export class EmptimesheetReportComponent extends BaseReportComponent implements OnInit, OnDestroy {

  _dataService: DataService;
  reportName = 'Timesheet By Employee Report';
  params: any;
  dateFrom: any;
  dateTo: any;
  datePipe: DatePipe = new DatePipe('en-AU');
  currentDate = new Date();
  reportData = new wjcCore.CollectionView();
  grid: wjcGrid.FlexGrid;

  reportLoading = false;
  TimesheetByEmployeeData: any[] = [];
  rosters: any[];
  pager: PaginationProperties;
  activePageItems: any[];
  companyName: string;
  printGrid: wjcGrid.FlexGrid;
  printGridInitialised = false;
  ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(@Inject(DataService) _dataService: DataService, private _paginationService: PaginationService,
   route: ActivatedRoute,  router: Router) {
    super(_dataService, route, router);
  }

  ngOnInit() {
    this.subscribeToRouteParams();

    this._dataService.reportFilterChanged.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      this.configureReportData(this.rosters);
    });

    this._dataService.reportPageChanged.pipe(takeUntil(this.ngUnsubscribe)).subscribe((page: number) => {
      this.setPage(page);
    });

    this._dataService.reportExcelExport.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      this.exportToExcel();
    });

    this._dataService.reportPrint.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      this.print();
    });
  }

  subscribeToRouteParams() {
    const filterData = {};
    this.params = this.route.params
      .subscribe(params => {
        if (params['ef']) { filterData['entitiesFilter'] = params['ef']; }
        if (params['et']) { filterData['entityType'] = params['et']; }
        if (params['from']) { filterData['dateFrom'] = params['from']; }
        if (params['to']) { filterData['dateTo'] = params['to']; }

        this.getRosters(filterData);
      }, error => {
        console.error(error);
        alert('Invalid search parameters for report');
        this.router.navigate(['report']);
    });
  }

  ngOnDestroy() {
    if (this.params) {
      this.params.unsubscribe();
    }
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this._paginationService.getPager(this.TimesheetByEmployeeData.length, page);

    // get current page of items
    this.activePageItems = this.TimesheetByEmployeeData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  getRosters(filterData: any): void {
    this._dataService.loading = true;
    const entitiesFilter = filterData.entitiesFilter;
    const entityType = filterData.entityType;
    const dateFrom = filterData.dateFrom;
    const dateTo = filterData.dateTo;

    this.dateFrom = dateFrom;
    this.dateTo = dateTo;

    this._dataService.getReportData(dateFrom, dateTo, entityType, entitiesFilter)
      .subscribe(rosters => {
        this.configureReportData(rosters);
        this._dataService.loading = false;
      }, error => {
        this._dataService.loading = false;
        error = error.json();
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
      });
  }

  sortReportGrid(): void {
    const cv = this.reportData;

    const NameSort = new wjcCore.SortDescription('employeeName', true);
    const DateSort = new wjcCore.SortDescription('startDate', true);
    const shiftEntityTypeId = new wjcCore.SortDescription('shiftEntityTypeId', true);
    const rosterTypeId = new wjcCore.SortDescription('rosterTypeId', true);
    const startTimeFull = new wjcCore.SortDescription('startTime', true);
    const shiftId = new wjcCore.SortDescription('shiftId', true);

    switch (this.userOptions['rosterOrder']) {
      case 0:
        cv.sortDescriptions.push(NameSort);
        cv.sortDescriptions.push(DateSort);
        cv.sortDescriptions.push(shiftEntityTypeId);
        cv.sortDescriptions.push(rosterTypeId);
        cv.sortDescriptions.push(startTimeFull);
        cv.sortDescriptions.push(shiftId);
        break;

      case 1:
        cv.sortDescriptions.push(NameSort);
        cv.sortDescriptions.push(DateSort);
        cv.sortDescriptions.push(startTimeFull);
        cv.sortDescriptions.push(shiftEntityTypeId);
        cv.sortDescriptions.push(rosterTypeId);
        cv.sortDescriptions.push(shiftId);
        break;

      case 2:
        cv.sortDescriptions.push(NameSort);
        cv.sortDescriptions.push(DateSort);
        cv.sortDescriptions.push(shiftId);
        cv.sortDescriptions.push(startTimeFull);
        cv.sortDescriptions.push(shiftEntityTypeId);
        cv.sortDescriptions.push(rosterTypeId);
        break;
    }
  }

  getTimesheetReportDataByEmployee(): void {
    const totalData = this.reportData.items;
    let count = 0;
    let filterTimesheet = [];
    const length = totalData.length, empDistinct = [], seen = new Set();
    outer:
    for (let index = 0; index < length; index++) {
      const value = totalData[index];
      if (seen.has(value.employeeFullName)) { continue outer; }
      seen.add(value.employeeFullName);
      empDistinct.push({
        employeeID: value.employeeID,
        employeeFullName: value.employeeFullName,
        shiftEntityTypeId: value.shiftEntityTypeId
      });
    }
    this.TimesheetByEmployeeData = [];
    for (const emp of empDistinct) {
      if (!(emp.employeeFullName in empDistinct)) {
        filterTimesheet = totalData.filter(data => data.employeeFullName === emp.employeeFullName);
        this.TimesheetByEmployeeData[count] = {
          employeeName: emp.employeeFullName,
          timesheetData: filterTimesheet
        };
        count = count + 1;
      } else {
        continue;
      }
    }
  }

  configureReportData(rosterToLoad: any[]): void {
    this.rosters = rosterToLoad;
    let filteredRosters = JSON.parse(JSON.stringify(this.filterRosterData(this.rosters)));
    filteredRosters = this.mapRosterData(filteredRosters);
    this.reportData.sourceCollection = filteredRosters;
    this.sortReportGrid();
    this.updateReportFormat();
    this.getTimesheetReportDataByEmployee();
    this.setPage(1);
    this._dataService.showReportLoadedToViewer.emit({
      pageCount: this.TimesheetByEmployeeData.length,
      reportName: this.reportName
    });
  }

  filterRosterData(rosters: any[]): any[] {
    const rosterType = this._dataService.reportSettingsFilter.rosterType[0];
    const filterroster = rosters.filter((roster) => {
      return ((roster.rosterTypeId === 1 && rosterType.planned)
            || (roster.rosterTypeId === 2 && rosterType.timekeeping)
            || (roster.rosterTypeId === 3 && rosterType.authorised));
    });
    return filterroster;
  }

  updateReportFormat(): void {
    const reportFormat = this._dataService.reportSettingsFilter.reportFormat[0];
    const cv = this.reportData;
    for (let i = 0; i < cv.items.length; i++) {
      // update based on report format.
      if (reportFormat.breakTime === false) {
        cv.items[i].hoursWorked = null;
        cv.items[i].breakStartFull = null;
        cv.items[i].breakEndFull = null;
        cv.items[i].breakLength = null;
      }
      if (reportFormat.hidEndTimes === true) {
        cv.items[i].endTime = null;
      }
    }
    this.reportData.refresh();
  }

  mapRosterData(rosters: any[]): any[] {
    // modifiy data types of response and add extra fields for grid display
    rosters = rosters.map((obj: any) => {

      // employee name without saluation and employee number
      if (obj.employeeFirstName && obj.employeeLastName) {
        obj.employeeFullName = `${obj.employeeLastName}, ${obj.employeeFirstName} ${obj.employeeSalutation} (${obj.employeeNumber}) `;
      } else { // skillset roster will have no first or last name but will have the grade name within `employeeName`
        obj.employeeFullName = obj.employeeName;
      }

      this.companyName = obj.companyName;
      if (obj.startDate) { obj.startDate = new Date(obj.startDate); }
      if (obj.endTime) { obj.endTime = new Date(obj.endTime); }
      if (obj.startTime) { obj.startTime = new Date(obj.startTime); }
      if (obj.breakStartFull) { obj.breakStartFull = new Date(obj.breakStartFull); }
      if (obj.breakEndFull) { obj.breakEndFull = new Date(obj.breakEndFull); }
      if (obj.endTime && obj.startTime) {
        const breakLen = obj.breakLength ? obj.breakLength : 0; // break length in minutes
        const engagementLen = moment.duration(moment(obj.endTime).diff(obj.startTime)); // engagement length as moment.duration object
        obj.hoursWorked = this.formatTime(engagementLen.subtract(breakLen, 'minutes'));
      }
        // remove unwanted properties from data obj
       const unwantedProperties = [
        'employeeLastName',
        'employeeFirstName',
        'employeeSalutation',
        'employeeNumber',
        'costCentreName',
        'gradeName',
        'positionName',
        'isSelected',
        'rosterID',
        'rosterNotes',
        'allowanceDetails',
        'allowanceAmount',
        'isBreakPaid',
        'rosterStartDate',
        'breakDuration',
        'assetName',
        'employeeStr'
      ];

      for (const prop in unwantedProperties) {
        delete obj[unwantedProperties[prop]];
      }

      return obj;
    });

    return rosters;
  }

  formatTime(duration): string {
    let hh: any = duration.hours();
    let mm: any = duration.minutes();

    if (hh < 10) {
      hh = '0' + hh;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }

    return `${hh}h ${mm}m`;
  }

  exportToExcel() {
    if (!this.printGridInitialised) {
      this.initialisePrintGrid();
    }
    const fileName = `Timesheet (By Employee) Report-${this.dateFrom}-${this.dateTo}.xlsx`;

    let timesheetGroup: wjcXlsx.Workbook = new wjcXlsx.Workbook();
    let timesheetWorkbook: wjcXlsx.Workbook = new wjcXlsx.Workbook();
    let groupName;
    const groupFont = new wjcXlsx.WorkbookFont();
    groupFont.bold = true;
    groupFont.size = 14;
    const groupStyle = new wjcXlsx.WorkbookStyle();
    groupStyle.font = groupFont;
    let groupRow = new wjcXlsx.WorkbookRow();
    this.printGrid.itemsSource = null;
    timesheetWorkbook = super.exportExcel(this.printGrid, true, null, true);

    for (let p = 0; p < this.TimesheetByEmployeeData.length; p++) {
      groupName = `Employee: ${this.TimesheetByEmployeeData[p].employeeName}`;
      this.printGrid.itemsSource = this.TimesheetByEmployeeData[p].timesheetData;
      groupRow = this.workbookRowBuilder(groupName);
      groupRow.cells.forEach((cell) => { cell.style = groupStyle; }); // add cell styling to header row
      timesheetWorkbook.sheets[0].rows.push(groupRow);
      timesheetGroup = super.exportExcel(this.printGrid, false, null, false);
      timesheetWorkbook.sheets[0].rows.push(...timesheetGroup.sheets[0].rows);
    }
    timesheetWorkbook.save(fileName);
  }

  initialisePrintGrid(): void { /* tslint:disable:max-line-length */
    this.printGrid = new wjcGrid.FlexGrid('#printGridHid', {
      autoGenerateColumns: false,
      columns: [
        { header: 'Location', binding: 'locationName', width: '*', minWidth: 150 },
        { header: 'Task', binding: 'TaskName', width: '*', minWidth: 100 },
        { header: 'Date', binding: 'startDate', align: 'right', width: '*', format: 'dd/MM/yyyy', mask: '00/00/0000', dataType: 'Date', minWidth: 100 },
        { header: 'Start', binding: 'startTime', align: 'right', width: '*', format: this.userSettingTimeformat, mask: '00:00', dataType: 'Date', minWidth: 100 },
        { header: 'Start Var', binding: '', align: 'right', width: '*' , minWidth: 100},
        { header: 'End', binding: 'endTime', align: 'right', width: '*', format: this.userSettingTimeformat, mask: '00:00', dataType: 'Date', minWidth: 100 },
        { header: 'End Var', binding: '', align: 'right', width: '*', minWidth: 100 },
        { header: 'Break', binding: 'breakLength', align: 'right', width: '*' },
        { header: 'Brk Start', binding: 'breakStartFull', align: 'right', width: '*', format: this.userSettingTimeformat, mask: '00:00', dataType: 'Date', minWidth: 100 },
        { header: 'Brk End', binding: 'breakEndFull', align: 'right', width: '*', format: this.userSettingTimeformat, mask: '00:00', dataType: 'Date', minWidth: 100 },
        { header: 'Hrs Wkd', binding: 'hoursWorked', align: 'right', width: '*', format: 'n2', minWidth: 100 },
        { header: 'Signature', binding: '', width: '*', minWidth: 100 },
        { header: 'Notes', binding: '', width: '*', minWidth: 100 }
      ]
    });
    this.printGridInitialised = true;
  } /* tslint:enable:max-line-length */

  print(): void {
    if (!this.printGridInitialised) {
      this.initialisePrintGrid();
    }
    const printDoc = new wjcCore.PrintDocument();
    const reportHeader = document.getElementById('printHeader').outerHTML;
    const reportFooter = document.getElementById('printFooter').outerHTML;
    const emptyTableFooter = `<tfoot class="print-footer-spacer"><tr><td>&nbsp;</td></tr></tfoot>`; // empty table footer for print spacing

    for (let i = 0; i < this.TimesheetByEmployeeData.length; i++) {
      printDoc.append(reportHeader);
      printDoc.append('<br>');
      printDoc.append(`<h4><span>${this.TimesheetByEmployeeData[i].employeeName}</span>` +
      `<span class="pull-right">NO SIGNATURE / NO PAY</span></h4>`);
      this.printGrid.itemsSource = this.TimesheetByEmployeeData[i].timesheetData;
      const gridAsTable = super.renderTable(this.printGrid, emptyTableFooter);
      printDoc.append(gridAsTable);
      printDoc.append(reportFooter); // add the actual report footer as a separate element
      printDoc.append('<div style="page-break-after:always;"></div>'); // force page break after each report section
    }
    printDoc.print();
  }
}
