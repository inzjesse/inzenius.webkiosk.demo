import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WjInputModule } from 'wijmo/wijmo.angular2.input';
import { WjGridModule } from 'wijmo/wijmo.angular2.grid';
import { ActivatedRoute, Router } from '@angular/router';

import { ActivatedRouteStub, DataServiceStub, PaginationServiceStub, RouterStub } from '../../../testing/helpers';
import { DataService } from '../../data.service';
import { PaginationService } from '../../pagination.service';
import { LoctimesheetReportComponent } from './loctimesheet-report.component';

describe('LoctimesheetReportComponent', () => {
  let component: LoctimesheetReportComponent;
  let fixture: ComponentFixture<LoctimesheetReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ WjInputModule, WjGridModule ],
      declarations: [ LoctimesheetReportComponent ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: DataService, useClass: DataServiceStub },
        { provide: Router, useClass: RouterStub },
        { provide: PaginationService, useClass: PaginationServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoctimesheetReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
