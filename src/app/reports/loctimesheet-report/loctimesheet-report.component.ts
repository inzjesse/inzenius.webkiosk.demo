import { Component, Inject, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as moment from 'moment';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import * as wjcCore from 'wijmo/wijmo';
import * as wjcInput from 'wijmo/wijmo.input';
import * as wjcGrid from 'wijmo/wijmo.grid';
import * as wjcPdf from 'wijmo/wijmo.pdf';
import * as wjcGridPdf from 'wijmo/wijmo.grid.pdf';
import * as wjcXlsx from 'wijmo/wijmo.xlsx';
import * as wjcGridXlsx from 'wijmo/wijmo.grid.xlsx';

import { DataService } from '../../data.service';
import { BaseReportComponent } from '../base-report';
import { CheckboxEvent } from '../../interfaces/checkbox-event';
import { PaginationService} from '../../pagination.service';
import { ReportSettings } from '../../interfaces/report-settings';
import { PaginationProperties } from '../../interfaces/pagination-properties';
import { SearchPipe } from '../../search.pipe';
import { HeadersVisibility } from 'wijmo/wijmo.grid';
import { DatePipe } from '@angular/common';
import { TaskColumn, PositionColumn } from '../../interfaces/optional-column';

@Component({
  selector: 'app-loctimesheet-report',
  templateUrl: './loctimesheet-report.component.html',
  styleUrls: ['./loctimesheet-report.component.scss']
})
export class LoctimesheetReportComponent extends BaseReportComponent implements OnInit, OnDestroy {
  _dataService: DataService;
  params: any;
  dateFrom: any;
  dateTo: any;
  reportData = new wjcCore.CollectionView();
  filterData = {};
  reportLoading = false;
  TimesheetByLocationData: any[] = [];
  reportSettingOptions: ReportSettings;
  allRosters: any[] = [];

  page = 1;
  terms = '';
  datePipe: DatePipe = new DatePipe('en-AU'); // TODO: don't hardcode locale
  companyName = '';

  ExportModeEnum = wjcGridPdf.ExportMode;
  PdfPageOrientationEnum = wjcPdf.PdfPageOrientation;
  ScaleModeEnum = wjcGridPdf.ScaleMode;

  exportMode = wjcGridPdf.ExportMode.All;
  orientation = wjcPdf.PdfPageOrientation.Portrait;
  scaleMode = wjcGridPdf.ScaleMode.ActualSize;
  currentDate = new Date();

  printGrid: wjcGrid.FlexGrid;
  printGridinitialise = false;
  ngUnsubscribe: Subject<void> = new Subject<void>();
  optionalColumn = TaskColumn;

  @ViewChild('dataGrid') reportgrid: wjcGrid.FlexGrid;

  // array of all items to be paged
  private allItems: any[];
  // pager object
  pager: PaginationProperties;
  pagedItems: any[];

  constructor(@Inject(DataService) _dataService: DataService, private _PaginationService: PaginationService,
   route: ActivatedRoute,  router: Router) {
    super(_dataService, route, router);
    this._dataService.showReportLoadedToViewer.emit(this);
  }

  ngOnInit() {
    this.configureSubscriptions();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    if (this.params) {
      this.params.unsubscribe();
    }
  }

  initGridLocal(grid: wjcGrid.FlexGrid, empName: string): void {
    this.reportgrid = grid;
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this._PaginationService.getPager(this.TimesheetByLocationData.length, page);

    // get current page of items
    this.pagedItems = this.TimesheetByLocationData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }


  configureSubscriptions() {
    this._dataService.reportFilterChanged.pipe(takeUntil(this.ngUnsubscribe)).subscribe((filter) => {
      this.updateReportData(this.allRosters);
    });

    this._dataService.reportPageChanged.pipe(takeUntil(this.ngUnsubscribe)).subscribe((filter) => {
      this.setPage(filter);
    });

    this._dataService.reportPrint.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      this.printReport();
    });

    this._dataService.reportSearchText.pipe(takeUntil(this.ngUnsubscribe)).subscribe((it) => {
      this.terms = it;
      console.log(it);
    });

    this._dataService.reportDownload.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      this.downloadPDF();
    });

    this._dataService.reportExcelExport.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
      this.exportToExcel();
    });

    this._dataService.reportZoomChanged.pipe(takeUntil(this.ngUnsubscribe)).subscribe((zoomLevel) => {
      if (this.reportgrid) { this.reportgrid.refresh(); }
    });

    this.params = this.route.params
      .subscribe(params => {
        if (params['ef']) { this.filterData['entitiesFilter'] = params['ef']; }
        if (params['et']) { this.filterData['entityType'] = params['et']; }
        if (params['from']) { this.filterData['dateFrom'] = params['from']; }
        if (params['to']) { this.filterData['dateTo'] = params['to']; }

        this._dataService.getUserReportDefaultSettingsFilter(this._dataService.selectedReport);
        this.getRosters(this.filterData);
      }, error => {
        console.error(error);
        alert('Invalid search parameters for report');
        this.router.navigate(['report']);
    });
  }

  getRosters(filterData: any): void {
    this._dataService.loading = true;
    const entitiesFilter = filterData.entitiesFilter;
    const entityType = filterData.entityType;
    const dateFrom = filterData.dateFrom;
    const dateTo = filterData.dateTo;
    let rosters: any[] = [];

    this.dateFrom = dateFrom;
    this.dateTo = dateTo;

    this._dataService.getReportData(dateFrom, dateTo, entityType, entitiesFilter)
      .subscribe(res => {
        rosters = res;
        this.updateReportData(rosters);
        this._dataService.loading = false;
        // initialize to page 1
      }, error => {
        this._dataService.loading = false;
        error = error.json();
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
      });
  }

  updateReportData(rosters: any[]) {
    this.updateReportColumns();
    this.allRosters = rosters;
    rosters = JSON.parse(JSON.stringify(this.filterRosterData( this.allRosters)));
    rosters = this.mapRosterData(rosters);
    this.reportData.sourceCollection = rosters;
    this.sortReportGrid();
    this.updateReportFormat();
    this.getTimesheetReportDataByLocation();
    this.setPage(1);
    this._dataService.showReportLoadedToViewer.emit(this);
  }

  updateReportColumns(): void {
    const reportColumns = this._dataService.reportSettingsFilter.colOptions[0];
    this.optionalColumn = reportColumns.position ? PositionColumn : TaskColumn;
  }

  filterRosterData(rosters: any[]): any[] {
    const rosterType = this._dataService.reportSettingsFilter.rosterType[0];
    const filterroster = rosters.filter((roster) => {
      return ((roster.rosterTypeId === 1 && rosterType.planned)
            || (roster.rosterTypeId === 2 && rosterType.timekeeping)
            || (roster.rosterTypeId === 3 && rosterType.authorised));
    });
    return filterroster;
  }

  mapRosterData(rosters: any[]): any[] {
    // modifiy data types of response and add extra fields for grid display
    rosters = rosters.map((obj: any) => {

      // employee name with saluation and employee number
      if (obj.employeeFirstName && obj.employeeLastName) {
        obj.employeeFullName = `${obj.employeeLastName}, ${obj.employeeFirstName} ${obj.employeeSalutation} (${obj.employeeNumber}) `;
      } else { // skillset roster will have no first or last name but will have the grade name within `employeeName`
        obj.employeeFullName = obj.employeeName;
      }
      this.companyName = obj.companyName;
      if (obj.startDate) { obj.startDate = new Date(obj.startDate); }
      if (obj.endTime) { obj.endTime = new Date(obj.endTime); }
      if (obj.startTime) { obj.startTime = new Date(obj.startTime); }
      if (obj.breakStartFull) { obj.breakStartFull = new Date(obj.breakStartFull); }
      if (obj.breakEndFull) { obj.breakEndFull = new Date(obj.breakEndFull); }
      if (obj.endTime && obj.startTime) {
        const breakLen = obj.breakLength ? obj.breakLength : 0; // break length in minutes
        const engagementLen = moment.duration(moment(obj.endTime).diff(obj.startTime)); // engagement length as moment.duration object
        obj.hoursWorked = this.formatTime(engagementLen.subtract(breakLen, 'minutes'));
      }

      // remove unwanted properties from data obj
      const unwantedProperties = [
        'employeeLastName',
        'employeeFirstName',
        'employeeSalutation',
        'employeeNumber',
        'costCentreName',
        'gradeName',
        'positionName',
        'isSelected',
        'rosterID',
        'rosterNotes',
        'allowanceDetails',
        'allowanceAmount',
        'isBreakPaid',
        'rosterStartDate',
        'breakDuration',
        'assetName',
        'employeeStr'
      ];

      for (const prop in unwantedProperties) {
        delete obj[unwantedProperties[prop]];
      }

      return obj;
    });

    return rosters;
  }

  updateReportFormat(): void {
    const reportFormat = this._dataService.reportSettingsFilter.reportFormat[0];
    const cv = this.reportData;
    for (let i = 0; i < cv.items.length; i++) {
      // update based on report format.
      if (reportFormat.breakTime === false) {
        cv.items[i].hoursWorked = null;
        cv.items[i].breakStartFull = null;
        cv.items[i].breakEndFull = null;
      }
      if (reportFormat.hidEndTimes === true) {
        cv.items[i].endTime = null;
      }
    }
    this.reportData.refresh();
  }

  formatTime(duration): string {
    let hh: any = duration.hours();
    let mm: any = duration.minutes();

    if (hh < 10) {
      hh = '0' + hh;
    }

    if (mm < 10) {
      mm = '0' + mm;
    }
    return `${hh}h ${mm}m`;
  }

  sortReportGrid(): void {
    const cv = this.reportData;

    const NameSort = new wjcCore.SortDescription('employeeName', true);
    const DateSort = new wjcCore.SortDescription('startDate', true);
    const shiftEntityTypeId = new wjcCore.SortDescription('shiftEntityTypeId', true);
    const rosterTypeId = new wjcCore.SortDescription('rosterTypeId', true);
    const startTimeFull = new wjcCore.SortDescription('startTime', true);
    const shiftId = new wjcCore.SortDescription('shiftId', true);

    switch (this.userOptions['rosterOrder']) {
      case 0:
        cv.sortDescriptions.push(NameSort);
        cv.sortDescriptions.push(DateSort);
        cv.sortDescriptions.push(shiftEntityTypeId);
        cv.sortDescriptions.push(rosterTypeId);
        cv.sortDescriptions.push(startTimeFull);
        cv.sortDescriptions.push(shiftId);
        break;

      case 1:
        cv.sortDescriptions.push(NameSort);
        cv.sortDescriptions.push(DateSort);
        cv.sortDescriptions.push(startTimeFull);
        cv.sortDescriptions.push(shiftEntityTypeId);
        cv.sortDescriptions.push(rosterTypeId);
        cv.sortDescriptions.push(shiftId);
        break;

      case 2:
        cv.sortDescriptions.push(NameSort);
        cv.sortDescriptions.push(DateSort);
        cv.sortDescriptions.push(shiftId);
        cv.sortDescriptions.push(startTimeFull);
        cv.sortDescriptions.push(shiftEntityTypeId);
        cv.sortDescriptions.push(rosterTypeId);
        break;
    }
  }

  getTimesheetReportDataByLocation(): void {
    const totalData = this.reportData.items;
    let count = 0;
    let filterTimesheet = [];
    const length = totalData.length, LocDistinct = [], seen = new Set();
    outer:
    for (let index = 0; index < length; index++) {
      const value = totalData[index];
      if (seen.has(value.locationName)) { continue outer; }
      seen.add(value.locationName);
      LocDistinct.push({locationName: value.locationName});
    }
    this.TimesheetByLocationData = [];
    for (const loc of LocDistinct) {
      if (!(loc.locationName in LocDistinct)) {
        filterTimesheet = totalData.filter(data => data.locationName === loc.locationName);
        this.TimesheetByLocationData[count] = {
          locationName: loc.locationName,
          timesheetData: filterTimesheet
        };
        count = count + 1;
      } else {
        continue;
      }
    }
  }

  downloadPDF() {
    const doc = new wjcPdf.PdfDocument({
      header: {
        declarative: {
          text: 'Timesheet (By Location) Report',
          font: new wjcPdf.PdfFont('times', 12),
          brush: '#bfc1c2'
        }
      },
      lineGap: 2,
      pageSettings: {
        layout: wjcPdf.PdfPageOrientation.Landscape,
        margins: {
          left: 36,
          right: 36,
          top: 36,
          bottom: 36
        }
      },
      ended: (sender, args: wjcPdf.PdfDocumentEndedEventArgs) => {
        wjcPdf.saveBlob(args.blob, 'Timesheet (By Location) Report.pdf');
      }
    });

    this._drawTimesheetReport(doc, () => doc.end());
  }

  private _drawTimesheetReport(doc: wjcPdf.PdfDocument, done: Function) {
    doc.header.drawText('header');
    doc.moveDown(2);
    const reportHeader = `${this.companyName} Timesheet (By Location) Report from ${moment(this.dateFrom).format('dd/MM/yyyy')} to `
      + `${moment(this.dateTo).format('dd/MM/yyyy')}`;
    const reportFooter  = document.getElementById('printFooter').innerHTML;
    doc.drawText(reportHeader);
    doc.moveDown(2);

    if (this.printGridinitialise === false) {
      this.initialisePrintGrid();
    }

    for (let p = 0; p < this.TimesheetByLocationData.length; p++) {
      this.printGrid.itemsSource = this.TimesheetByLocationData[p].timesheetData;
      wjcGridPdf.FlexGridPdfConverter.draw(this.printGrid, doc, doc.width , doc.height, {
        styles: {
          cellStyle: {
            backgroundColor: '#ffffff',
            borderColor: '#c6c6c6'
          },
          altCellStyle: {
            backgroundColor: '#f9f9f9'
          },
          groupCellStyle: {
            font: <any>{ weight: 'bold' },
            backgroundColor: '#dddddd'
          },
          headerCellStyle: {
            backgroundColor: '#eaeaea'
          }
        }
      });
      doc.moveDown(2);
    }

    doc.drawText(reportFooter);
    doc.footer.drawText('footer');
    done();
  }

  initialisePrintGrid(): void { /* tslint:disable:max-line-length */
    this.printGrid = new wjcGrid.FlexGrid('#printGridHid', {
      autoGenerateColumns: false,
      columns: [
          { header: 'Employee', binding: 'employeeName', align: 'left', width: '*', minWidth: 150 },
          { header: 'Event', binding: 'eventName', align: 'left', width: '*' , minWidth: 100},
          { header: this.optionalColumn.columnName, binding: this.optionalColumn.columnBinding, align: 'left', width: '*' , minWidth: 100},
          { header: 'Date', binding: 'startDate', align: 'right', width: '*', format: 'dd/MM/yyyy', mask: '00/00/0000', dataType: 'Date', minWidth: 100 },
          { header: 'Start', binding: 'startTime', align: 'right', width: '*', format: this.userSettingTimeformat, mask: '00:00', dataType: 'Date', minWidth: 100 },
          { header: 'Start Var', binding: '', align: 'right', width: '*' , minWidth: 100},
          { header: 'End', binding: 'endTime', align: 'right', width: '*', format: this.userSettingTimeformat, mask: '00:00', dataType: 'Date', minWidth: 100 },
          { header: 'End Var', binding: '', align: 'right', width: '*', minWidth: 100 },
          { header: 'Break', binding: 'breakLength', align: 'right', width: '*' },
          { header: 'Brk Start', binding: 'breakStartFull', align: 'right', width: '*', format: this.userSettingTimeformat, mask: '00:00', dataType: 'Date', minWidth: 100 },
          { header: 'Brk End', binding: 'breakEndFull', align: 'right', width: '*', format: this.userSettingTimeformat, mask: '00:00', dataType: 'Date', minWidth: 100 },
          { header: 'Hrs Wkd', binding: 'hoursWorked', align: 'right', width: '*', format: 'n2', minWidth: 100 },
          { header: '',  width: '*' , minWidth: 100},
          { header: 'Signature', binding: '', align: 'left', width: '*' , minWidth: 100},
          { header: 'Notes', binding: '', align: 'left', width: '*', minWidth: 100 },
      ]
    });
    this.printGridinitialise = true;
  } /* tslint:enable:max-line-length */

  printReport() {
    const printDoc = new wjcCore.PrintDocument();
    if (this.printGridinitialise === false) {
      this.initialisePrintGrid();
    }
    printDoc.title = '\u00A0';

    const reportHeader = document.getElementById('printHeader').outerHTML;
    const reportFooter  = document.getElementById('printFooter').outerHTML;
    const emptyTableFooter = `<tfoot class="print-footer-spacer"><tr><td>&nbsp;</td></tr></tfoot>`; // empty table footer for print spacing

    for (let p = 0; p < this.TimesheetByLocationData.length; p++) {
      printDoc.append(reportHeader);
      printDoc.append('<br>');
      printDoc.append(`<h4><span>Location: &nbsp;&nbsp;&nbsp;&nbsp; ${this.TimesheetByLocationData[p].locationName}</span>` +
                      `<span class="pull-right">NO SIGNATURE / NO PAY</span></h4>`);
      this.printGrid.itemsSource = this.TimesheetByLocationData[p].timesheetData;
      printDoc.append(super.renderTable(this.printGrid, emptyTableFooter));
      printDoc.append(reportFooter);
      printDoc.append('<div style="page-break-after: always;"></div>');
    }
    printDoc.print();
  }

  exportToExcel() {
    if (this.printGridinitialise === false) {
      this.initialisePrintGrid();
    }
    const fileName = `Timesheet (By Location) Report-${this.dateFrom}-${this.dateTo}.xlsx`;

    let timesheetGroup: wjcXlsx.Workbook = new wjcXlsx.Workbook();
    let timesheetWorkbook: wjcXlsx.Workbook = new wjcXlsx.Workbook();
    let groupName;
    const groupFont = new wjcXlsx.WorkbookFont();
    groupFont.bold = true;
    groupFont.size = 14;
    const groupStyle = new wjcXlsx.WorkbookStyle();
    groupStyle.font = groupFont;
    let groupRow = new wjcXlsx.WorkbookRow();
    this.printGrid.itemsSource = null;
    timesheetWorkbook = super.exportExcel(this.printGrid, true, null, true);

    for (let p = 0; p < this.TimesheetByLocationData.length; p++) {
      this.printGrid.itemsSource = this.TimesheetByLocationData[p].timesheetData;

      groupName = `Location: ${this.TimesheetByLocationData[p].locationName}`;
      groupRow = this.workbookRowBuilder(groupName);
      groupRow.cells.forEach((cell) => { cell.style = groupStyle; }); // add cell styling to header row
      timesheetWorkbook.sheets[0].rows.push(groupRow);
      timesheetGroup = super.exportExcel(this.printGrid, false, null, false);
      timesheetWorkbook.sheets[0].rows.push(...timesheetGroup.sheets[0].rows);
    }
    super.resizeWorkbookColumnsToContentSize(timesheetWorkbook);
    timesheetWorkbook.save(fileName);
  }

  workbookRowBuilder(val: any): wjcXlsx.WorkbookRow {
    const row = new wjcXlsx.WorkbookRow();
    row.cells.push(this.workbookCellBuilder(val));

    return row;
  }

  // create a workbook cell containing the given value
  workbookCellBuilder(val: any): wjcXlsx.WorkbookCell {
    const cell = new wjcXlsx.WorkbookCell();
    cell.value = val;
    return cell;
  }
}
