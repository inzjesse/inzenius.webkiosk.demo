import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterReportComponent } from './filter-report.component';
import { DataServiceStub } from '../../../../testing/helpers';
import { DataService } from '../../../data.service';
import { FormsModule } from '@angular/forms'; // (ngModel)

describe('FilterReportComponent', () => {
  let component: FilterReportComponent;
  let fixture: ComponentFixture<FilterReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ FilterReportComponent ],
      providers: [
        { provide: DataService, useClass: DataServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
