import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import { DataService } from '../../../data.service';
import { ReportSettings } from '../../../interfaces/report-settings';
import { CheckboxEvent } from '../../../interfaces/checkbox-event';

@Component({
  selector: 'app-filter-report',
  templateUrl: './filter-report.component.html',
  styleUrls: ['./filter-report.component.css']
})
export class FilterReportComponent implements OnInit, OnDestroy {
  _dataService: DataService;
  reportSettingOptions: ReportSettings;
  report = 'Select Report';
  rosterOption = 'planned';

  constructor(@Inject(DataService) _dataService: DataService) {
    this._dataService = _dataService;
    this.SubscribeOnLoadShowfilterReportPopup();
  }

  ngOnInit() {
    this.getReportSettings();
  }

  ngOnDestroy() {
  }

  SubscribeOnLoadShowfilterReportPopup(): void {
    this._dataService.showFilterReportPopup.subscribe(() => {
      this.report = this._dataService.selectedReport;
      this.getReportSettings();
    });
  }

  getReportSettings() {
    this.reportSettingOptions = this._dataService.getUserReportDefaultSettingsFilter(this.report);
  }

  updateReportSettings(event: CheckboxEvent, arr: ReportSettings) {
    switch (this.report) {
      case 'Timesheet By Location Report':
        this.clearRosterTypeSettings(arr);
        arr.rosterType[0] = { planned: false, timekeeping: false, authorised: false };

        switch (this.rosterOption) {
          case 'timekeeping':
            arr.rosterType[0].timekeeping = true;
            break;
          case 'authorised':
            arr.rosterType[0].authorised = true;
            break;
          default:
            arr.rosterType[0].planned = true;
            break;
        }
        break;
     default:
        break;
    }
    this._dataService.updateUserReportSettingsFilter(arr);
  }

  toggleCheckbox(event) {
    switch (this.report) {
      case 'Timesheet By Location Report':
        switch (this.rosterOption) {
          case 'timekeeping':
            this.reportSettingOptions.rosterType[0].timekeeping = true;
            this.reportSettingOptions.rosterType[0].authorised = false;
            this.reportSettingOptions.rosterType[0].planned = false;
            (event.target as HTMLInputElement).checked =  this.reportSettingOptions.rosterType[0].timekeeping;
            break;
          case 'authorised':
            this.reportSettingOptions.rosterType[0].authorised = true;
            this.reportSettingOptions.rosterType[0].timekeeping = false;
            this.reportSettingOptions.rosterType[0].planned = false;
            (event.target as HTMLInputElement).checked =  this.reportSettingOptions.rosterType[0].authorised;
            break;
          default:
              this.reportSettingOptions.rosterType[0].planned = true;
              this.reportSettingOptions.rosterType[0].authorised = false;
              this.reportSettingOptions.rosterType[0].timekeeping = false;
              (event.target as HTMLInputElement).checked =  this.reportSettingOptions.rosterType[0].planned;
              break;
          }
          break;
        default:
        break;
      }
  }

  clearRosterTypeSettings(arr: ReportSettings) {
    this.reportSettingOptions.rosterType[0] = { planned: false, timekeeping: false, authorised: false };
  }

  updateReportFilter() {
    this._dataService.reportFilterChanged.emit();
  }
}
