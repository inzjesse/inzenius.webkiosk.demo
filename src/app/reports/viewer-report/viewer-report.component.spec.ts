import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, EventEmitter, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { WjInputModule } from 'wijmo/wijmo.angular2.input';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcInput from 'wijmo/wijmo.input';

import { DataService } from '../../data.service';
import { Reportcommands } from '../../reportcommands';
import { ActivatedRouteStub, DataServiceStub, RouterStub, ReportcommandsStub } from '../../../testing/helpers';
import { ViewerReportComponent } from './viewer-report.component';
import { FilterReportComponent } from '../report-settings/filter-report/filter-report.component';
import { ReportComponent } from '../../report/report.component';
import { ReportRoutes } from '../../report/report.routing';

describe('ViewerReportComponent', () => {
  let component: ViewerReportComponent;
  let fixture: ComponentFixture<ViewerReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, WjInputModule ],
      declarations: [
        ViewerReportComponent,
        FilterReportComponent,
        ReportComponent],
      providers: [
        { provide: Reportcommands, useClass: ReportcommandsStub },
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: DataService, useClass: DataServiceStub },
        { provide: Router, useClass: RouterStub }
      ],
      schemas: [ NO_ERRORS_SCHEMA ] // ignore unknown elements for child components
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should `disable` the toolbar when enable toggle is set to false', () => {
    // flag should default to false
    expect(component._dataService.enableToolbar).toBeFalsy();

    // css class 'disabledtoolbar' should exist on toolbar div
    const toolbarDiv = fixture.debugElement.query(By.css('.disabledtoolbar'));
    expect(toolbarDiv).toBeTruthy();
  });

  it('should `enable` the toolbar when enable toggle is set to true', () => {
    component._dataService.enableToolbar = true;
    expect(component._dataService.enableToolbar).toBeTruthy();

    fixture.detectChanges();

    // css class 'disabledtoolbar' should not exist on toolbar div
    const toolbarDiv = fixture.debugElement.query(By.css('.disabledtoolbar'));
    expect(toolbarDiv).toBeFalsy();
  });
});
