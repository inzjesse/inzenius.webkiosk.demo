import { AfterViewInit, Component, Inject, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcInput from 'wijmo/wijmo.input';

import { Reportcommands } from '../../reportcommands';
import { DataService } from '../../data.service';

import { ActivatedRoute, Router } from '@angular/router';
import { ReportRoutes } from '../../report/report.routing';

@Component({
  selector: 'app-viewer-report',
  templateUrl: './viewer-report.component.html',
  styleUrls: ['./viewer-report.component.scss']
})
export class ViewerReportComponent implements OnInit, AfterViewInit {
  @ViewChild('reportView') private reportView: ElementRef;
  _dataService: DataService;
  reportName = 'Select Report';
  totalPages = 1;
  zoomLevels: wjcCore.CollectionView;
  currentPage = 1;
  pager: any = {};
  searchBar = false;
  searchTextinReport: string;
  pagedItems: any[];


  constructor(@Inject(DataService) _dataService: DataService, public reportcommands: Reportcommands) {
    this._dataService = _dataService;
    this._dataService.enableToolbar = false;
    this._dataService.refreshViewer.subscribe(() => {
      this._dataService.enableToolbar = false;
      this.reportName = this._dataService.selectedReport;
      this.totalPages = 1;
      this.currentPage = 1;
    });
    this._dataService.showReportLoadedToViewer.subscribe((x) => {
      this._dataService.enableToolbar = true;
      this.reportName = this._dataService.selectedReport;
      this.totalPages = 1;
      this.currentPage = 1;
      if (x.pageCount > 0) {
        this.totalPages = x.pageCount;
      }
    });
  }

  ngOnInit() {
    this.zoomLevels = new wjcCore.CollectionView([
      { header: '25%', value: 0.25 },
      { header: '50%', value: 0.5 },
      { header: '75%', value: 0.75 },
      { header: '100%', value: 1 },
      { header: '125%', value: 1.25 },
      { header: '150%', value: 1.5 },
      { header: '175%', value: 1.75 },
      { header: '200%', value: 2 }
    ], {
      currentChanged: (s, e) => {
        const view = this.reportView.nativeElement;
        if (view) {
            view.style.zoom = s.currentItem.value;
            this._dataService.reportZoomChanged.emit(s.currentItem.value);
        }
      }
    });
  }

  ngAfterViewInit() {
    this.zoomLevels.moveCurrentToPosition(3);
  }

  setPage(page: number) {
    page = Number(page.toString());
    if (page < 1 || page > this.totalPages ) {
      alert('There is no page number ' + page + ' in this document.');
      return;
    }
    this.currentPage = page ;
    this._dataService.reportPageChanged.emit(this.currentPage);
  }

  print() {
    this._dataService.reportPrint.emit();
  }

  downloadPDF() {
    this._dataService.reportDownload.emit();
    // wjcGridPdf.FlexGridPdfConverter.export(this.flexGrid, 'FlexGrid.pdf', {
    // 	maxPages: 10,
    // 	exportMode: this.exportMode,
    // 	scaleMode: this.scaleMode,
    // 	documentOptions: {
    // 		pageSettings: {
    // 			layout: this.orientation
    // 		},
    // 		header: {
    // 			declarative: {
    // 				text: '\t&[Page]\\&[Pages]'
    // 			}
    // 		},
    // 		footer: {
    // 			declarative: {
    // 				text: '\t&[Page]\\&[Pages]'
    // 			}
    // 		},
    // 		info: {
    // 			author: 'C1',
    // 			title: 'PdfDocument sample',
    // 			keywords: 'PDF, C1, sample',
    // 			subject: 'PdfDocument'
    // 		}
    // 	},
    // 	styles: {
    // 		cellStyle: {
    // 			backgroundColor: '#ffffff',
    // 			borderColor: '#c6c6c6'
    // 		},
    // 		altCellStyle: {
    // 			backgroundColor: '#f9f9f9'
    // 		},
    // 		groupCellStyle: {
    // 			backgroundColor: '#dddddd'
    // 		},
    // 		headerCellStyle: {
    // 			backgroundColor: '#eaeaea'
    // 		}
    // 	}
    // });
  }

  ExcelExport() {
    this._dataService.reportExcelExport.emit();
  }

  showSearchBar() {
    this.searchBar = !(this.searchBar);
  }

  searchText(text: string) {
    this.searchTextinReport = text;
    this._dataService.reportSearchText.emit(this.searchTextinReport);
  }
}
