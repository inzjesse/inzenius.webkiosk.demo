import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RosterGridComponent } from './roster-grid.component';
import { DataServiceStub } from '../../testing/helpers';

import { WjInputModule } from 'wijmo/wijmo.angular2.input';
import { WjGridModule } from 'wijmo/wijmo.angular2.grid';
import { FormsModule } from '@angular/forms';

import { DataService } from '../data.service';

describe('RosterGridComponent', () => {
  let component: RosterGridComponent;
  let fixture: ComponentFixture<RosterGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, WjInputModule, WjGridModule ],
      declarations: [ RosterGridComponent ],
      providers: [
        { provide: DataService, useClass: DataServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RosterGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
