import { Component, OnInit, Input, Inject, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import * as wjcCore from 'wijmo/wijmo';
import * as wjcGrid from 'wijmo/wijmo.grid';
import * as wjcInput from 'wijmo/wijmo.input';

import { DataService } from '../data.service';

// This class will be extended to accomodate all roster grids
export class CustomMerge extends wjcGrid.MergeManager {

  getMergedRange(p: wjcGrid.GridPanel, r: number, c: number, clip?: boolean) {

    const rng = super.getMergedRange(p, r, c, clip);
    const shiftCompositionColumn = 1;
    const dateColumn = 2;

    // Merge shift composition and date cells only when the rows have the same shiftId
    if (rng) {
      const shiftId = p.rows[r].dataItem.shiftId;
      while ((c === shiftCompositionColumn || c === dateColumn) && rng.row < r && p.rows[rng.row].dataItem.shiftId !== shiftId) {
        rng.row++;
      }
      while ((c === shiftCompositionColumn || c === dateColumn) && rng.row2 > r && p.rows[rng.row2].dataItem.shiftId !== shiftId) {
        rng.row2--;
      }
    }

    return rng;
  }
}

@Component({
  selector: 'app-roster-grid',
  templateUrl: './roster-grid.component.html',
  styleUrls: ['./roster-grid.component.css']
})
export class RosterGridComponent implements OnInit, OnDestroy {
  @Input() itemsSource = new wjcCore.CollectionView();
  @Input() selectionMode = wjcGrid.SelectionMode.None; // default to None selection mode

  filterData: any = null;
  editingCellCurrentValue: any = null;
  editingCellNewValue: any = null;
  currentRosterEntities: any = null;
  userOptions: {} = null;
  userSettingTimeformat = 'HH:mm';
  updatedData = [];
  ngUnsubscribe: Subject<void> = new Subject<void>();
  _dataService: DataService;

  constructor(@Inject(DataService) _dataService: DataService) {
    this._dataService = _dataService;
    // this.getUserOptions();
    // this._dataService.suppRosterGridData = new wjcCore.CollectionView();
    this._dataService.showSuppGrid.pipe(takeUntil(this.ngUnsubscribe)).subscribe((res: any) => {
      this.itemsSource.refresh();
    }, error => {
      alert('Error while trying to show rosters');
    });
    this._dataService.changeColumnOption.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => this.changeColumnOptions());
  }
  getUserOptions() {
    this._dataService.getUserOptions()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(userOptions => {
        this.userOptions = userOptions;
        this.getUserSettingsTimeFormat();
      }, error => {
        error = error.error;
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this._dataService.loading = false;
  }

  getTimeWithoutTz(time: any) {
    const temp = time.toString().replace('T', ' ');
    return new Date(temp.substr(0, 19));
  }

  convertMinsToHrsMins(minutes: number) {
    let h: any = Math.floor(minutes / 60);
    let m: any = minutes % 60;
    h = h < 10 ? '0' + h : h;
    m = m < 10 ? '0' + m : m;
    return h + ':' + m;
  }

  getUserSettingsTimeFormat() {

    switch (this.userOptions['timeFormat']) {
      case 11:
        // twentyFourHourTime
        this.userSettingTimeformat = 'HH:mm';
        break;
      case 9:
        // twelveHourTimeSingleLowercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm t';
        break;
      case 7:
        // twelveHourTimeDoubleLowercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm tt';
        break;
      case 8:
        // twelveHourTimeSingleUppercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm t';
        break;
      case 6:
        // twelveHourTimeDoubleUppercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm tt';
        break;
      default:
        // 24 hour format
        this.userSettingTimeformat = 'HH:mm';
    }
  }

  backupCellData(event: wjcGrid.CellRangeEventArgs) {
    const grid = event.panel.grid;
    this.editingCellCurrentValue = grid.getCellData(event.row, event.col, true);
  }

  styleCell(event: wjcGrid.FormatItemEventArgs) {
    const panel = event.panel;
    const currentRow: wjcGrid.Row = panel.grid.rows[event.row];
    const grid = panel.grid;
    const colBinding = grid.columns[event.col] ? grid.columns[event.col].binding : null;

    if (colBinding === null) { return; } // grid header row not initialised yet

    if (panel.cellType === wjcGrid.CellType.Cell && event.col >= 4) {
      switch (currentRow.dataItem.rosterTypeId) {
        case 2:
          wjcCore.addClass(event.cell, 'roster-type2');
          break;
        case 3:
          wjcCore.addClass(event.cell, 'roster-type3');
          break;
      }
      return;
    }

    // shift composition icons
    if (panel.cellType === wjcGrid.CellType.Cell && colBinding === 'shiftCompositionCodeDisplay') {
      wjcCore.addClass(event.cell, this.getShiftCompositionStyleByCode(currentRow.dataItem.shiftCompositionCode));

      event.cell.innerHTML = '<div><span>' + event.cell.innerHTML + '</span></div>';

      // add css that centres the shift composition icon
      wjcCore.setCss(event.cell, {
        display: 'table',
        tableLayout: 'fixed'
      });
      wjcCore.setCss(event.cell.children[0], {
        display: 'table-cell',
        verticalAlign: 'middle'
      });

      return;
    }

    // roster acceptance type icons
    if (panel.cellType === wjcGrid.CellType.Cell && colBinding === 'rosterAcceptanceTypeDisplay') {
      wjcCore.addClass(event.cell, this.getRosterAcceptanceStyleByCode(currentRow.dataItem.rosterAcceptanceTypeDisplay));

      event.cell.innerHTML = '<div><span>' + event.cell.innerHTML + '</span></div>';
      return;
    }

    // vertically centre-align merged cells in name and date columns
    if (grid.itemsSource.length && grid.getMergedRange(grid.cells, event.row, event.col)) {
      event.cell.innerHTML = '<div>' + event.cell.innerHTML + '</div>';
      wjcCore.setCss(event.cell, {
        display: 'table',
        tableLayout: 'fixed'
      });
      wjcCore.setCss(event.cell.children[0], {
        display: 'table-cell',
        verticalAlign: 'middle'
      });
    }
  }

  initGrid(grid: wjcGrid.FlexGrid) {
    this._dataService.suppRosterGrid = grid;

    // set custom cell merging
    grid.mergeManager = new CustomMerge(grid);

    // set frozen columns
    if (this.userOptions['employeeName'] && this.userOptions['date']) {
      grid.frozenColumns = 4;
    } else if (this.userOptions['employeeName'] || this.userOptions['date']) {
      grid.frozenColumns = 3;
    }

  }

  // determine css required for each shift composition code
  getShiftCompositionStyleByCode(shiftCompositionCode: string): string {
    switch (shiftCompositionCode) {
      case 'PTA':
        return 'shift-composition-pta';
      case 'PTx':
        return 'shift-composition-pt';
      case 'PxA':
        return 'shift-composition-pa';
      case 'xTA':
        return 'shift-composition-ta';
      case 'Pxx':
        return 'roster-icon-p';
      case 'xTx':
        return 'roster-icon-t';
      case 'xxA':
        return 'roster-icon-a';
      default:
        return '';
    }
  }
  getRosterAcceptanceStyleByCode(rosterAcceptanceCode: string): string {
    switch (rosterAcceptanceCode) {
      case 'Pr':
        return 'roster-icon-pre';
      case 'Pb':
        return 'roster-icon-pub';
      case 'W':
        return 'roster-icon-w';
      case 'T':
        return 'roster-icon-t';
      case 'A':
        return 'roster-icon-a';
      default:
        return '';
    }
  }
  changeColumnOptions() {
    this.getUserOptions();
  }
  ngOnInit() {
  }

}
