import { AfterViewChecked, Component, ElementRef, Inject, OnDestroy, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';

import { DataService } from '../../data.service';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements AfterViewChecked, OnDestroy {
  @ViewChild('log') private log: ElementRef;

  logMessages: string[] = [];
  logExists = false;
  _dataService: DataService;
  enableScrolling = false;
  ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(@Inject(DataService) _dataService: DataService) {
    this._dataService = _dataService;
    this._dataService.logMessage.pipe(takeUntil(this.ngUnsubscribe)).subscribe((message: string) => {
      const date = new Date().toTimeString().split(' ')[0];
      this.logMessages.push(`[${date}] ${message}`);
      this.enableScrolling = true;
      this.logExists = true;
     }, error => {
      alert('Error while trying to log message');
    });
  }

  ngAfterViewChecked() {
    this.scrollLogDown();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  scrollLogDown() {
    if (this.enableScrolling) {
      this.log.nativeElement.scrollTop = this.log.nativeElement.scrollHeight;
    }
  }

  onScroll() {
    this.enableScrolling = (this.log.nativeElement.scrollHeight - this.log.nativeElement.scrollTop) === this.log.nativeElement.clientHeight;
  }

  clearLog() {
    this.logMessages.length = 0;
    this.logExists = false;
  }

}
