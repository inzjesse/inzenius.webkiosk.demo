import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';


import { DataService } from '../../data.service';
import { Commands } from '../../commands';
import { CommandsStub, DataServiceStub } from '../../../testing/helpers';
import { RosterToolbarComponent } from './roster-toolbar.component';

describe('RosterToolbarComponent', () => {
  let component: RosterToolbarComponent;
  let fixture: ComponentFixture<RosterToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RosterToolbarComponent ],
      providers: [
        { provide: Commands, useClass: CommandsStub },
        { provide: DataService, useClass: DataServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RosterToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
