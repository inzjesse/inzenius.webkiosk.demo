import { Component, Inject, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { retry, takeUntil } from 'rxjs/operators';
import { forkJoin } from 'rxjs/observable/forkJoin';
import * as wjcCore from 'wijmo/wijmo';

import { DataService } from '../../data.service';
import { Commands } from '../../commands';

@Component({
  selector: 'app-roster-toolbar',
  templateUrl: './roster-toolbar.component.html',
  styleUrls: ['./roster-toolbar.component.css']
})
export class RosterToolbarComponent implements OnDestroy {

  _dataService: DataService;
  toggleRowsFilter: number[] = [1, 2, 3]; // PTA
  totalPlannedTime = '00:00';
  totalTimekeepingTime = '00:00';
  totalAuthorisedTime = '00:00';
  toggleP = false;
  toggleT = false;
  toggleA = false;
  showEmployeeDetails = false;
  ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(@Inject(DataService) _dataService: DataService, public commands: Commands) {
    this._dataService = _dataService;
    this._dataService.showRosterTimes.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => this.rosterTimes());
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  authoriseSelectedRosters() {
    let rostersToAuthorise: any[] = [];

    // filter the user selection to only include rosters which are valid for authorisation
    rostersToAuthorise = this._dataService.rosterGrid.selectedItems.filter((roster) => {
      return (roster.rosterTypeId === 1 || roster.rosterTypeId === 2);
    });

    this.authoriseRosters(rostersToAuthorise);
  }

  toggleRows(rosterTypeId?: number) {
    if (rosterTypeId) {
      const filterIndex = this.toggleRowsFilter.indexOf(rosterTypeId);
      if (filterIndex > -1) {
        this.toggleRowsFilter.splice(filterIndex, 1);
      } else {
        this.toggleRowsFilter.push(rosterTypeId);
      }
    } else {
      this.toggleRowsFilter = [1, 2, 3];
    }

    this._dataService.gridData.filter = item => {
      for (const filter of this.toggleRowsFilter) {
        if (item.rosterTypeId === filter) {
          return true;
        }
      }
    };

    this.rosterTimes();
  }

  rosterTimes() {
    let planned = 0;
    let timekeeping = 0;
    let authorised = 0;

    for (const item of this._dataService.gridData.items) {
      switch (item.rosterTypeId) {
        case 1:
          planned += item.engagementLengthFull;
          continue;

        case 2:
          timekeeping += item.engagementLengthFull;
          continue;

        case 3:
          authorised += item.engagementLengthFull;
          continue;
      }
    }

    this.totalPlannedTime = this.convertMinsToHrsMins(planned);
    this.totalTimekeepingTime = this.convertMinsToHrsMins(timekeeping);
    this.totalAuthorisedTime = this.convertMinsToHrsMins(authorised);
  }

  convertMinsToHrsMins(minutes: number) {
    let h: any = Math.floor(minutes / 60);
    let m: any = minutes % 60;
    h = h < 10 ? '0' + h : h;
    m = m < 10 ? '0' + m : m;
    return h + ':' + m;
  }

  smartAuthorisationFilter(option: string) {
    if (option === 'all') {
      this.toggleRows();
    }

    if (option === 'notAuthorized') {
      this._dataService.gridData.filter = roster => {
        return this.canRosterBeAuthorised(roster);
      };
    }

    if (option === 'authorized') {
      this._dataService.gridData.filter = roster => this.isRosterAuthorised(roster);
    }

    this.rosterTimes();

    return this._dataService.gridData;
  }

  /** Return true if a roster belongs to a shift that has been authorised */
  isRosterAuthorised(roster: any): boolean {
    return roster.shiftCompositionCode.includes('A');
  }

  /** Return true for any planned or timekeeping roster that has not yet been authorised */
  canRosterBeAuthorised(roster: any): boolean {
    return !this.isRosterAuthorised(roster) && (roster.rosterTypeId === 1 || roster.rosterTypeId === 2);
  }

  /** Authorise all planned rosters (loaded in grid) that have not yet been authorised */
  bulkAuthorize(): void {
    const rostersNotAuthorised = this._dataService.gridData.sourceCollection.filter(roster => {
      return (roster.rosterTypeId === 1 || roster.rosterTypeId === 2);
     });

    const rostersToAuthorise: any[] = [];

    // filter the list further to only include planned rosters
    for (const roster of rostersNotAuthorised) {
      if (roster.rosterTypeId === 1) { // planned rosters
        rostersToAuthorise.push(roster);
      }
    }

    this.authoriseRosters(rostersToAuthorise);
  }

  authoriseRosters(rosters: any[]): void {
    const rosterRequests: Observable<any>[] = [];

    rosters.forEach((roster) => {
      // prepare API request for each roster to be authorised
      const req = this._dataService.authoriseRoster(roster.rosterId).pipe(retry(2)); // retry in case of API overload
      rosterRequests.push(req);
    });

    // exit method if no rosters to authorise
    if (rosterRequests.length === 0) { return; }

    // will be set to false on roster grid reload after API requests (regardless of success/fail)
    this._dataService.loading = true;

    this._dataService.logMessage.emit(`Authorising ${rosterRequests.length} roster${rosterRequests.length === 1 ? '' : 's'}.`);

    // process all of the requests and then reload roster grid
    forkJoin(...rosterRequests).pipe(takeUntil(this.ngUnsubscribe)).subscribe(response => {
      this._dataService.logMessage.emit(`Authorised ${response.length} roster${response.length === 1 ? '' : 's'}.`);

      // reload grid
      this._dataService.showRosters.emit(this._dataService.filterData);
    }, error => {
      // at least one authorisation failed - tell the user and reload the roster grid
      this._dataService.logMessage.emit(`At least one roster failed to be authorised.`);
      alert('At least one roster failed to be authorised.');

      this._dataService.showRosters.emit(this._dataService.filterData);
    });
  }
}
