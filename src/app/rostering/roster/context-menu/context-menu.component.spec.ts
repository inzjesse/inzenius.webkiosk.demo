import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { WjInputModule } from 'wijmo/wijmo.angular2.input';

import { CommandsStub, DataServiceStub } from '../../../../testing/helpers';
import { DataService } from '../../../data.service';
import { Commands } from '../../../commands';
import { ContextMenuComponent } from './context-menu.component';

describe('ContextMenuComponent', () => {
  let component: ContextMenuComponent;
  let fixture: ComponentFixture<ContextMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ WjInputModule ],
      declarations: [ ContextMenuComponent ],
      providers: [
        { provide: Commands, useClass: CommandsStub },
        { provide: DataService, useClass: DataServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // Cannot get this unit test to succeed yet
  // Template error `Cannot read property 'directive' of null` but I suspect
  // that this error is masking the "real" error somewhere else
  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
