import { Component } from '@angular/core';

import { Commands } from '../../../commands';

@Component({
  selector: 'app-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.css']
})
export class ContextMenuComponent {

  constructor(public commands: Commands) { }

}
