import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { WjInputModule } from 'wijmo/wijmo.angular2.input';
import { WjGridModule } from 'wijmo/wijmo.angular2.grid';
import { WjNavModule } from 'wijmo/wijmo.angular2.nav';

import { DataService } from '../../data.service';
import { Commands } from '../../commands';
import { ActivatedRouteStub, CommandsStub, DataServiceStub } from '../../../testing/helpers';
import { RosterComponent } from './roster.component';
import { ContextMenuComponent } from './context-menu/context-menu.component';
import { TreeViewComponent } from '../../tree/tree-view/tree-view.component';

describe('RosterComponent', () => {
  let component: RosterComponent;
  let fixture: ComponentFixture<RosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, WjInputModule, WjGridModule, WjNavModule ],
      declarations: [
        RosterComponent,
        ContextMenuComponent,
        TreeViewComponent
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
        { provide: Commands, useClass: CommandsStub },
        { provide: DataService, useClass: DataServiceStub }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
