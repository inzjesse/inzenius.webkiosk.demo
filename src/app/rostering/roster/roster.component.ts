import { Component, Inject, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcGrid from 'wijmo/wijmo.grid';
import * as wjcInput from 'wijmo/wijmo.input';


import { DataService } from '../../data.service';

export class CustomMerge extends wjcGrid.MergeManager {

  getMergedRange(p: wjcGrid.GridPanel, r: number, c: number, clip?: boolean) {

    const rng = super.getMergedRange(p, r, c, clip);
    const shiftCompositionColumn = 0;
    const dateColumn = 1;

    // Merge shift composition and date cells only when the rows have the same shiftId
    if (rng) {
      const shiftId = p.rows[r].dataItem.shiftId;
      while ((c === shiftCompositionColumn || c === dateColumn) && rng.row < r && p.rows[rng.row].dataItem.shiftId !== shiftId) {
        rng.row++;
      }
      while ((c === shiftCompositionColumn || c === dateColumn) && rng.row2 > r && p.rows[rng.row2].dataItem.shiftId !== shiftId) {
        rng.row2--;
      }
    }

    return rng;
  }
}

@Component({
  selector: 'app-roster',
  templateUrl: './roster.component.html',
  styleUrls: ['./roster.component.css']
})
export class RosterComponent implements OnDestroy {
  _dataService: DataService;
  filterData: any = null;
  editingCellCurrentValue: any = null;
  editingCellNewValue: any = null;
  currentRosterEntities: any = null;
  userOptions: {} = null;
  userSettingTimeformat = 'HH:mm';
  updatedData = [];
  ngUnsubscribe: Subject<void> = new Subject<void>();
  showEmployeeDetails = false;

  constructor(_activatedRoute: ActivatedRoute, @Inject(DataService) _dataService: DataService) {
    this._dataService = _dataService;
    this.getUserOptions();
    this._dataService.gridData = new wjcCore.CollectionView(); // reset roster grid on rostering page load

    this._dataService.showRosters.pipe(takeUntil(this.ngUnsubscribe)).subscribe((res: any) => {
      this.filterData = res;
      this.getRosters(this.filterData);
    }, error => {
      alert('Error while trying to show rosters');
    });
    this._dataService.changeColumnOption.pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => this.changeColumnOptions());
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
    this._dataService.loading = false;
  }

  getRosters(filterData: any) {
    this._dataService.isAddEngagement = true;
    this._dataService.loading = true;
    const entitiesFilter = filterData.entitiesFilter;
    const entityType = filterData.entityType;
    const dateFrom = filterData.dateFrom;
    const dateTo = filterData.dateTo;
    let requestTimestamp: number = null;
    let responseTimestamp: number = null;

    this._dataService.logMessage.emit(`Retrieving rosters for ${entityType} selected between ${dateFrom} and ${dateTo}`);

    requestTimestamp = new Date().getTime();
    this._dataService.getRosters(entitiesFilter, entityType, dateFrom, dateTo)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(rosterList => {

        // transform dates
        rosterList = rosterList.map(roster => {
          if (roster.startDateFull) { roster.startDateFull = this.getTimeWithoutTz(roster.startDateFull); }
          if (roster.startTimeFull) { roster.startTimeFull = this.getTimeWithoutTz(roster.startTimeFull); }
          if (roster.endTimeFull) { roster.endTimeFull = this.getTimeWithoutTz(roster.endTimeFull); }
          if (roster.breakStartFull) { roster.breakStartFull = this.getTimeWithoutTz(roster.breakStartFull); }
          if (roster.breakEndFull) { roster.breakEndFull = this.getTimeWithoutTz(roster.breakEndFull); }

          return roster;
        });

        this._dataService.gridData.sourceCollection = rosterList;

        responseTimestamp = new Date().getTime();
        const elapsedSeconds = Math.floor((responseTimestamp - requestTimestamp) / 1000);

        const msg = `${rosterList.length} engagements retrieved in ${elapsedSeconds} ${elapsedSeconds === 1 ? 'second' : 'seconds'}`;
        this._dataService.logMessage.emit(msg);
        this._dataService.showRosterTimes.emit();
        this._dataService.loading = false;
      }, error => {
        this._dataService.loading = false;
        error = error.error;
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
      });

  }

  changeColumnOptions() {
    this.getUserOptions();
  }

  getTimeWithoutTz(time: any) {
    const temp = time.toString().replace('T', ' ');
    return new Date(temp.substr(0, 19));
  }

  convertMinsToHrsMins(minutes: number) {
    let h: any = Math.floor(minutes / 60);
    let m: any = minutes % 60;
    h = h < 10 ? '0' + h : h;
    m = m < 10 ? '0' + m : m;
    return h + ':' + m;
  }

  getUserSettingsTimeFormat() {

    switch (this.userOptions['timeFormat']) {
      case 11:
        // twentyFourHourTime
        this.userSettingTimeformat = 'HH:mm';
        break;
      case 9:
        // twelveHourTimeSingleLowercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm t';
        break;
      case 7:
        // twelveHourTimeDoubleLowercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm tt';
        break;
      case 8:
        // twelveHourTimeSingleUppercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm t';
        break;
      case 6:
        // twelveHourTimeDoubleUppercaseAMPMIndicator
        this.userSettingTimeformat = 'hh:mm tt';
        break;
      default:
        // 24 hour format
        this.userSettingTimeformat = 'HH:mm';
    }
  }

  backupCellData(event: wjcGrid.CellRangeEventArgs) {
    const grid = event.panel.grid;
    this.editingCellCurrentValue = grid.getCellData(event.row, event.col, true);
  }

  checkDataChange(event: wjcGrid.CellRangeEventArgs) {
    const grid = event.panel.grid;
    this.editingCellNewValue = grid.getCellData(event.row, event.col, true);

    if (this.editingCellCurrentValue !== this.editingCellNewValue) {
      const header: string = event.panel.columns[event.col].header;
      const rowData = this._dataService.gridData.currentEditItem;

      if (header === 'Start Time') { this.updatedData.push({ paramName: 'startTime', paramValue: this.getValidStartTime(rowData) }); }
      if (header === 'Finish Time') { this.updatedData.push({ paramName: 'finishTime', paramValue: this.getValidFinishTime(rowData) }); }

      if (header === 'Break Start') {
        // if Break Start is null then Break Finish should also be null
        if (!this.editingCellNewValue) {
          this.updatedData.push({ paramName: 'breakStartTime', paramValue: null });
          this.updatedData.push({ paramName: 'breakFinishTime', paramValue: null });
          rowData.breakEndFull = null;
          grid.refresh();
        } else {
          this.updatedData.push({ paramName: 'breakStartTime', paramValue: this.getValidBreakStart(rowData) });
        }
      }

      if (header === 'Break Finish') {
        // if Break Finish is null then Break Start should also be null
        if (!this.editingCellNewValue) {
          this.updatedData.push({ paramName: 'breakFinishTime', paramValue: null });
          this.updatedData.push({ paramName: 'breakStartTime', paramValue: null });
          rowData.breakStartFull = null;
          grid.refresh();
        } else {
          this.updatedData.push({ paramName: 'breakFinishTime', paramValue: this.getValidBreakFinish(rowData) });
        }
      }

      if (header === 'Break Paid') {
        this.updatedData.push({ paramName: 'isBreakPaid', paramValue: grid.getCellData(event.row, event.col, false) });
      }

      if (header === 'Break Length') {
        // if Break Length is deleted then Break Start/Finish should be deleted too
        if (!this.editingCellNewValue) {
          this.updatedData.push({ paramName: 'breakFinishTime', paramValue: null });
          this.updatedData.push({ paramName: 'breakStartTime', paramValue: null });
          rowData.breakStartFull = null;
          rowData.breakEndFull = null;
          grid.refresh();
        } else {
          const midDate = new Date((rowData.startTimeFull.getTime() + rowData.endTimeFull.getTime()) / 2);
          const newBreakStart = new Date(midDate.getTime() - (rowData.breakLength / 2) * 60000);
          const newBreakFinish = new Date(midDate.getTime() + (rowData.breakLength / 2) * 60000);

          this.updatedData.push({ paramName: 'breakStartTime', paramValue: newBreakStart });
          this.updatedData.push({ paramName: 'breakFinishTime', paramValue: newBreakFinish });
        }
      }

      if (header === 'Location') { this.updatedData.push({ paramName: 'locationId', paramValue: this.getSelectedEntityId() }); }
      if (header === 'Grade') { this.updatedData.push({ paramName: 'gradeId', paramValue: this.getSelectedEntityId() }); }
      if (header === 'Rule') { this.updatedData.push({ paramName: 'ruleIds', paramValue: this.getSelectedEntitiesIds() }); }
      if (header === 'Cost Centre') { this.updatedData.push({ paramName: 'costCentreId', paramValue: this.getSelectedEntityId() }); }
      if (header === 'Event') { this.updatedData.push({ paramName: 'eventId', paramValue: this.getSelectedEntityId() }); }
      if (header === 'Position') { this.updatedData.push({ paramName: 'positionId', paramValue: this.getSelectedEntityId() }); }
      if (header === 'Tasks') { this.updatedData.push({ paramName: 'taskIds', paramValue: this.getSelectedEntitiesIds() }); }
      if (header === 'Assets') { this.updatedData.push({ paramName: 'assetIds', paramValue: this.getSelectedEntitiesIds() }); }
      if (header === 'Allowances') { this.updatedData.push({ paramName: 'allowanceIds', paramValue: this.getSelectedEntitiesIds() }); }
      if (header === 'Deductions') { this.updatedData.push({ paramName: 'deductionIds', paramValue: this.getSelectedEntitiesIds() }); }
      if (header === 'Notes') {
        this.updatedData.push({ paramName: 'rosterRowNotes', paramValue: grid.getCellData(event.row, event.col, false) });
      }
    }
  }

  sendDataChanges() {
    if (this.updatedData.length) {
      this._dataService.loading = true;
      const rowData = this._dataService.gridData.currentEditItem;

      this._dataService.updateRoster(rowData.rosterId, this.updatedData)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(res => {
          if (rowData.rosterTypeId === 3 && rowData.isSigned) {
            this._dataService.logMessage.emit(`An Authorised engagement has been altered for ${rowData.employeeName} `
              + `on ${rowData.startDate}, ${rowData.startDateFull.getFullYear()}. The signature assigned has been cleared.`);
          }
          this._dataService.logMessage.emit(`Updated engagement for ${rowData.employeeName} `
            + `on ${rowData.startDate}, ${rowData.startDateFull.getFullYear()}`);
          this.updatedData = [];
          this._dataService.loading = false;
        }, err => {
          this._dataService.loading = false;
          this._dataService.logMessage.emit(`Unable to update engagement for ${rowData.employeeName} `
            + `on ${rowData.startDate}, ${rowData.startDateFull.getFullYear()}`);
        });
    }
  }

  getValidStartTime(rowData: any) {
    const newStartTime: Date = rowData.startTimeFull;
    const rosterDate: Date = rowData.startDateFull;
    const rosterDateCopy = new Date(rosterDate.getTime());

    const newStartTimeHour = newStartTime.getHours();
    const newStartTimeMinutes = newStartTime.getMinutes();

    rosterDateCopy.setHours(newStartTimeHour);
    rosterDateCopy.setMinutes(newStartTimeMinutes);

    // return rosterDateCopy.toISOString();
    return rosterDateCopy.toString().substring(0, 24).replace('T', ' ');
  }

  getValidFinishTime(rowData: any) {
    const newFinishTime: Date = rowData.endTimeFull;
    const rosterDate: Date = rowData.startDateFull;
    const rosterDateCopy = new Date(rosterDate.getTime());

    const newFinishTimeHour = newFinishTime.getHours();
    const newFinishTimeMinutes = newFinishTime.getMinutes();

    rosterDateCopy.setHours(newFinishTimeHour);
    rosterDateCopy.setMinutes(newFinishTimeMinutes);

    const startTime: Date = rowData.startTimeFull;
    if (startTime) {
      // if finish time is before the start time then add 1 day
      if (rosterDateCopy < startTime) {
        rosterDateCopy.setDate(rosterDateCopy.getDate() + 1);
      }
    }

    // return rosterDateCopy.toISOString();
    return rosterDateCopy.toString().substring(0, 24).replace('T', ' ');
  }

  getValidBreakStart(rowData: any) {
    const newBreakStart: Date = rowData.breakStartFull;
    const rosterDate: Date = rowData.startDateFull;
    const rosterDateCopy = new Date(rosterDate.getTime());

    const newBreakStartHour = newBreakStart.getHours();
    const newBreakStartMinutes = newBreakStart.getMinutes();

    rosterDateCopy.setHours(newBreakStartHour);
    rosterDateCopy.setMinutes(newBreakStartMinutes);

    const startTime: Date = rowData.startTimeFull;
    if (startTime) {
      // if break start is before the start time then add 1 day
      if (rosterDateCopy < startTime) {
        rosterDateCopy.setDate(rosterDateCopy.getDate() + 1);
      }
    }

    // return rosterDateCopy.toISOString();
    return rosterDateCopy.toString().substring(0, 24).replace('T', ' ');
  }

  getValidBreakFinish(rowData: any) {
    const newBreakFinish: Date = rowData.breakEndFull;
    const rosterDate: Date = rowData.startDateFull;
    const rosterDateCopy = new Date(rosterDate.getTime());

    const newBreakFinishHour = newBreakFinish.getHours();
    const newBreakFinishMinutes = newBreakFinish.getMinutes();

    rosterDateCopy.setHours(newBreakFinishHour);
    rosterDateCopy.setMinutes(newBreakFinishMinutes);

    const startTime: Date = rowData.startTimeFull;
    if (startTime) {
      // if break finish is before the start time then add 1 day
      if (rosterDateCopy < startTime) {
        rosterDateCopy.setDate(rosterDateCopy.getDate() + 1);
      }
    }

    // return rosterDateCopy.toISOString();
    return rosterDateCopy.toString().substring(0, 24).replace('T', ' ');
  }

  getSelectedEntityId() {
    for (const entity of this.currentRosterEntities) {
      if (entity.entityName === this.editingCellNewValue) {
        return entity.entityId;
      }
    }
    return null;
  }

  getSelectedEntitiesIds() {
    const entityIds: number[] = [];
    const selectedEntities = this.editingCellNewValue.split(', ');

    for (const selectedValue of selectedEntities) {
      for (const entity of this.currentRosterEntities) {
        if (entity.entityName === selectedValue) {
          entityIds.push(entity.entityId);
          break;
        }
      }
    }

    return entityIds;
  }

  styleCell(event: wjcGrid.FormatItemEventArgs) {
    const panel = event.panel;
    const currentRow: wjcGrid.Row = panel.grid.rows[event.row];
    const grid = panel.grid;
    const colBinding = grid.columns[event.col] ? grid.columns[event.col].binding : null;

    if (colBinding === null) { return; } // grid header row not initialised yet

    if (panel.cellType === wjcGrid.CellType.Cell && event.col >= 3) {
      switch (currentRow.dataItem.rosterTypeId) {
        case 2:
          wjcCore.addClass(event.cell, 'roster-type2');
          break;
        case 3:
          wjcCore.addClass(event.cell, 'roster-type3');
          break;
      }
      return;
    }

    // shift composition icons
    if (panel.cellType === wjcGrid.CellType.Cell && colBinding === 'shiftCompositionCodeDisplay') {
      wjcCore.addClass(event.cell, this.getShiftCompositionStyleByCode(currentRow.dataItem.shiftCompositionCode));

      event.cell.innerHTML = '<div><span>' + event.cell.innerHTML + '</span></div>';

      // add css that centres the shift composition icon
      wjcCore.setCss(event.cell, {
        display: 'table',
        tableLayout: 'fixed'
      });
      wjcCore.setCss(event.cell.children[0], {
        display: 'table-cell',
        verticalAlign: 'middle'
      });

      return;
    }

    // roster acceptance type icons
    if (panel.cellType === wjcGrid.CellType.Cell && colBinding === 'rosterAcceptanceTypeDisplay') {
      wjcCore.addClass(event.cell, this.getRosterAcceptanceStyleByCode(currentRow.dataItem.rosterAcceptanceTypeDisplay));

      event.cell.innerHTML = '<div><span>' + event.cell.innerHTML + '</span></div>';
      return;
    }

    // vertically centre-align merged cells in name and date columns
    if (grid.itemsSource.length && grid.getMergedRange(grid.cells, event.row, event.col)) {
      event.cell.innerHTML = '<div>' + event.cell.innerHTML + '</div>';
      wjcCore.setCss(event.cell, {
        display: 'table',
        tableLayout: 'fixed'
      });
      wjcCore.setCss(event.cell.children[0], {
        display: 'table-cell',
        verticalAlign: 'middle'
      });
    }
  }

  getRosterEntities(cell: any, comboBox: wjcInput.ComboBox) {
    comboBox.isReadOnly = true;
    comboBox.placeholder = 'Loading...';

    const rosterEntities = new wjcCore.CollectionView();
    comboBox.itemsSource = rosterEntities;

    const entityType: string = cell.col.header;
    const rosterId: number = cell.item.rosterId;

    this._dataService.getRosterEntities(entityType, rosterId)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(rosterEntityList => {
        this.currentRosterEntities = rosterEntityList;
        rosterEntities.sourceCollection = rosterEntityList;
        comboBox.displayMemberPath = 'entityName';

        if (this.editingCellCurrentValue) {
          comboBox.text = this.editingCellCurrentValue;
        } else if (!comboBox.isRequired) {
          comboBox.selectedValue = null;
          comboBox.placeholder = 'Select';
        }
        comboBox.isReadOnly = false;
      }, error => {
        error = error.error;
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
      });
  }

  getRosterEntitiesMulti(cell: any, multiSelect: wjcInput.MultiSelect, idsArrayName: string) {
    const rosterEntities = new wjcCore.CollectionView();
    multiSelect.itemsSource = rosterEntities;
    multiSelect.placeholder = 'Loading...';

    const entityType: string = cell.col.header;
    const rosterId: number = cell.item.rosterId;

    this._dataService.getRosterEntities(entityType, rosterId)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(rosterEntityList => {
        this.currentRosterEntities = rosterEntityList;

        // set the options already checked
        const alreadySelected = cell.item[idsArrayName];

        for (const entity of this.currentRosterEntities) {
          for (const selectedId of alreadySelected) {
            if (entity.entityId === selectedId) {
              entity.checked = true;
            }
          }
        }

        rosterEntities.sourceCollection = this.currentRosterEntities;
        multiSelect.displayMemberPath = 'entityName';
        multiSelect.checkedMemberPath = 'checked';
        multiSelect.headerFormat = '{count} options selected';
        multiSelect.placeholder = 'Select';

        multiSelect.checkedItemsChanged.addHandler(() => {
          const checkedItems: string[] = [];

          for (const item of multiSelect.checkedItems) {
            checkedItems.push(item.entityName);
          }

          cell.value = checkedItems.join(', ');
        });
      }, error => {
        error = error.error;
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
      });
  }

  isAuthorizable(event: wjcGrid.CellRangeEventArgs) {
    const grid = event.panel.grid;
    let shiftUnauthorisedRosters: any[] = [];
    let shiftAuthorisedRosters: any[] = [];
    let isAuthorise = false;
    if (grid.selectedItems.length === 1) {
      shiftUnauthorisedRosters = grid.itemsSource.sourceCollection.filter(roster => {
          if (roster.shiftId === grid.selectedItems[0].shiftId && (roster.rosterTypeId === 1 || roster.rosterTypeId === 2)) {
            return true;
          }
      });
      shiftAuthorisedRosters = grid.itemsSource.sourceCollection.filter(roster => {
        if (roster.shiftId === grid.selectedItems[0].shiftId && roster.rosterTypeId === 3) {
          return true;
        }
      });
    }

    if (shiftUnauthorisedRosters.length > 0) {
      if (shiftUnauthorisedRosters.length === 1 && shiftUnauthorisedRosters.length === shiftAuthorisedRosters.length) {
        isAuthorise = false;
      } else {
        isAuthorise = true;
      }
    } else {
      const authorisableRosters: any[] = grid.selectedItems.filter((roster) => {
        if (!roster.shiftCompositionCode.includes('A') && (roster.rosterTypeId === 1 || roster.rosterTypeId === 2)) {
          return true;
        }
      });
      if (authorisableRosters.length > 0) {
        isAuthorise = true;
      }
    }
     return isAuthorise;
  }

  resetPbButtons(event: wjcGrid.CellRangeEventArgs) {
    const grid = event.panel.grid;
    let otherThanPlanned: any = false;

    const selectedRosters: any[] = grid.selectedItems.filter((roster) => {
      if (roster.rosterTypeId === 2 || roster.rosterTypeId === 3) {
        otherThanPlanned = true;
        return true;
      }
    });
    this._dataService.disableWorkingEng = selectedRosters.length > 0;
    this._dataService.disablePrepublishedgEng = selectedRosters.length > 0;
    this._dataService.disablePublishedEng = selectedRosters.length > 0;

    if (otherThanPlanned === false) {
      const selectedwRosters: any[] = grid.selectedItems.filter((roster) => {
        if (roster.rosterTypeId === 1 && roster.rosterAcceptanceTypeId === 1) {
          return true;
        }
      });

      const selectedPrRosters: any[] = grid.selectedItems.filter((roster) => {
        if (roster.rosterTypeId === 1 && roster.rosterAcceptanceTypeId === 2) {
          return true;
        }
      });

      const selectedPbRosters: any[] = grid.selectedItems.filter((roster) => {
        if (roster.rosterTypeId === 1 && roster.rosterAcceptanceTypeId === 3) {
          return true;
        }
      });

      if (selectedwRosters.length === grid.selectedItems.length) {
        this._dataService.disableWorkingEng = true;
      }
      if (selectedPrRosters.length === grid.selectedItems.length) {
        this._dataService.disablePrepublishedgEng = true;
      }
      if (selectedPbRosters.length === grid.selectedItems.length) {
        this._dataService.disablePublishedEng = true;
      }
    }
  }

  getUserOptions() {
    this._dataService.getUserOptions()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(userOptions => {
        this.userOptions = userOptions;
        this.getUserSettingsTimeFormat();
      }, error => {
        error = error.error;
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
    });
  }

  sortGrid() {
    const cv = this._dataService.gridData;
    cv.sortDescriptions.clear();

    const employeeName = new wjcCore.SortDescription('employeeName', true);
    const startDateFull = new wjcCore.SortDescription('startDateFull', true);
    const shiftEntityTypeId = new wjcCore.SortDescription('shiftEntityTypeId', true);
    const rosterTypeId = new wjcCore.SortDescription('rosterTypeId', true);
    const startTimeFull = new wjcCore.SortDescription('startTimeFull', true);
    const shiftId = new wjcCore.SortDescription('shiftId', true);

    switch (this.userOptions['rosterOrder']) {
      case 0:
        cv.sortDescriptions.push(employeeName);
        cv.sortDescriptions.push(startDateFull);
        cv.sortDescriptions.push(shiftEntityTypeId);
        cv.sortDescriptions.push(rosterTypeId);
        cv.sortDescriptions.push(startTimeFull);
        cv.sortDescriptions.push(shiftId);
        break;

      case 1:
        cv.sortDescriptions.push(employeeName);
        cv.sortDescriptions.push(startDateFull);
        cv.sortDescriptions.push(startTimeFull);
        cv.sortDescriptions.push(shiftEntityTypeId);
        cv.sortDescriptions.push(rosterTypeId);
        cv.sortDescriptions.push(shiftId);
        break;

      case 2:
        cv.sortDescriptions.push(employeeName);
        cv.sortDescriptions.push(startDateFull);
        cv.sortDescriptions.push(shiftId);
        cv.sortDescriptions.push(startTimeFull);
        cv.sortDescriptions.push(shiftEntityTypeId);
        cv.sortDescriptions.push(rosterTypeId);
        break;
    }
  }

  initGrid(grid: wjcGrid.FlexGrid) {
    this._dataService.rosterGrid = grid;

    // set frozen columns
    if (this.userOptions['employeeName'] && this.userOptions['date']) {
      grid.frozenColumns = 4;
    } else if (this.userOptions['employeeName'] || this.userOptions['date']) {
      grid.frozenColumns = 3;
    }

    // set custom cell merging
    grid.mergeManager = new CustomMerge(grid);

    this._dataService.gridData.sourceCollectionChanged.addHandler(() => {
      this.sortGrid();
      grid.selectionMode = wjcGrid.SelectionMode.ListBox;
    });

    // temporary workaround for Wijmo issue TFS ID 229996
    // http://wijmo.com/topic/bug-editing-a-cell-and-clicking-inside-the-grid-does-not-track-data-change/
    grid.hostElement.addEventListener('click', (e) => {
      const ht = grid.hitTest(e.pageX, e.pageY);
      if (wjcGrid.CellType[ht.cellType] === 'None') {
        const rangeArgs = new wjcGrid.CellRangeEventArgs(grid.cells, grid.viewRange);
        grid.onRowEditEnding(rangeArgs);
      }
    });
  }

  // determine css required for each shift composition code
  getShiftCompositionStyleByCode(shiftCompositionCode: string): string {
    switch (shiftCompositionCode) {
      case 'PTA':
        return 'shift-composition-pta';
      case 'PTx':
        return 'shift-composition-pt';
      case 'PxA':
        return 'shift-composition-pa';
      case 'xTA':
        return 'shift-composition-ta';
      case 'Pxx':
        return 'roster-icon-p';
      case 'xTx':
        return 'roster-icon-t';
      case 'xxA':
        return 'roster-icon-a';
      default:
        return '';
    }
  }

  // determine roster acceptance icon letter by acceptance type and roster type
  getRosterAcceptanceStyleByCode(rosterAcceptanceCode: string): string {
    switch (rosterAcceptanceCode) {
      case 'Pr':
        return 'roster-icon-pre';
      case 'Pb':
        return 'roster-icon-pub';
      case 'W':
        return 'roster-icon-w';
      case 'T':
        return 'roster-icon-t';
      case 'A':
        return 'roster-icon-a';
      default:
        return '';
    }
  }

  setSelectedEntityID() {
    const rowData = this._dataService.gridData.currentItem;
    if (rowData) {
      if (this._dataService.supplementaryGridEntity !== rowData.employeeId) {
        this._dataService.supplementaryGridEntity = rowData.employeeId;
        this._dataService.showSuppGrid.emit();
      }
    }
  }
}
