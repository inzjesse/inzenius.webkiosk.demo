import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { DataService } from '../data.service';
import { DataServiceStub } from '../../testing/helpers';
import { RosteringComponent } from './rostering.component';

describe('RosteringComponent', () => {
  let component: RosteringComponent;
  let fixture: ComponentFixture<RosteringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RosteringComponent ],
      providers: [
        { provide: DataService, useClass: DataServiceStub }
      ],
      schemas: [ NO_ERRORS_SCHEMA ] // ignore unknown html tags (<app-log> etc)
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RosteringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
