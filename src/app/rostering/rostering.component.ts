import { Component, Inject, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { DataService } from '../data.service';

@Component({
  selector: 'app-rostering',
  templateUrl: './rostering.component.html',
  styleUrls: ['./rostering.component.css']
})
export class RosteringComponent implements AfterViewInit {
  showTree = true;
  _dataService: DataService;
  @ViewChild('roster') private roster: ElementRef;

  constructor(@Inject(DataService) _dataService: DataService) {
    this._dataService = _dataService;
  }

  ngAfterViewInit() {
    // refresh grid on class toggle
    const observer = new MutationObserver(mutations => {
      mutations.forEach(mutation => this._dataService.rosterGrid.refresh());
    });

    const config = { attributes: true, childList: true, characterData: true };
    observer.observe(this.roster.nativeElement, config);
  }
}
