import {Component, Input, Output, EventEmitter} from '@angular/core';

import { TreeEntity } from '../../interfaces/tree-entity';

@Component({
  selector: 'app-tree-view',
  templateUrl: './tree-view.component.html',
  styleUrls: ['./tree-view.component.css']
})
export class TreeViewComponent {
    @Input() treeData: any[];
    @Input() buttonType = 'checkbox'; // default to checkbox
    @Output() checkboxChange = new EventEmitter<boolean>();
    @Output() radioChange = new EventEmitter();

    sendCheckboxEvent() {
        this.checkboxChange.emit();
    }

    sendRadioEvent(entityId: number) {
        this.radioChange.emit(entityId);
  }

    toggleDescendants(entityNode: TreeEntity): void {
        // toggle selected value for this node
        entityNode.selected = !entityNode.selected;

        // set all bold descendants to same value as this node
        this.toggleChildren(entityNode.children, entityNode.selected);
    }

    toggleChildren(entityList: TreeEntity[], selected: boolean) {
        for (const entity of entityList) {
            entity.selected = selected;

            // select any children
            if (entity.children.length) {
                this.toggleChildren(entity.children, selected);
            }
        }
        this.sendCheckboxEvent();
    }

    clickEntityButton(button) {
        if (this.buttonType === 'radio') {
            // ensure only one entity node can be `selected` when using radio buttons
            this.unselectAllNodes(this.treeData);

            button.selected = true;
        } else { // checkbox
            button.selected = !button.selected;
        }
    }

    unselectAllNodes(entityList) {
        for (const node of entityList) {
            node.selected = false;

            // do the same for any children
            if (node.children != null) {
                this.unselectAllNodes(node.children);
            }
        }
    }
}
