import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import * as wjcCore from 'wijmo/wijmo';
import * as wjcInput from 'wijmo/wijmo.input';
import * as moment from 'moment';

import { DataService } from '../data.service';
import { TreeViewComponent } from './tree-view/tree-view.component';
import { TreeEntity } from '../interfaces/tree-entity';
import { CheckboxEvent } from '../interfaces/checkbox-event';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnDestroy {

  _dataService: DataService;
  dateFrom = this.getDateFrom();
  dateTo = this.getDateTo();
  entityTypeList = new wjcCore.CollectionView();
  entityTypeListLoaded = false;
  expandTreeModel = false;
  myTree: TreeEntity[] = [];
  selectedEntities = '';

  ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(_dataService: DataService) {
    this._dataService = _dataService;
    this._dataService.loading = false;
    this.subscribeToChangePeriodEvents();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  subscribeToChangePeriodEvents(): void {
    this._dataService.showPreviousPeriod.subscribe(() => {
      this.prevPeriod();
    });
    this._dataService.showNextPeriod.subscribe(() => {
      this.nextPeriod();
    });
  }

  getDateFrom(): Date {
    const savedDate = localStorage.getItem('rosteringDateFrom');
    if (savedDate) { return new Date(savedDate); }
    return new Date(); // Today
  }

  getDateTo(): Date {
    const savedDate = localStorage.getItem('rosteringDateTo');
    if (savedDate) { return new Date(savedDate); }

    const date = new Date();
    date.setDate(date.getDate() + 1); // Tomorrow
    return date;
  }

  getEntityTypes(comboBox: wjcInput.ComboBox): void {
    this._dataService.getEntityTypes()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(entityTypesResponse => {
        this.entityTypeList.sourceCollection = entityTypesResponse;

        const savedEntityType = localStorage.getItem('rosteringEntityType');
        if (savedEntityType) { comboBox.text = savedEntityType; }

        this.getTreeEntities(comboBox.text);
        this.entityTypeListLoaded = true;
      }, error => {
        error = error.error;
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
    });
  }

  formatDate(dateObject: Date): string {
    const year  = dateObject.getFullYear();
    let month = <any>dateObject.getMonth() + 1;
    let day   = <any>dateObject.getDate();

    if (month < 10) {
      month = '0' + month;
    }

    if (day < 10) {
      day = '0' + day;
    }

    return `${year}/${month}/${day}`;
  }

  getTreeEntities(selectedEntityType: string): void {
    // uncheck Expand tree checkbox
    this.expandTreeModel = false;

    localStorage.setItem('rosteringEntityType', selectedEntityType);
    this.myTree = [];
    this.selectedEntities = '';

    // cancel pending tree entity requests only if the entity types are finished loading
    // this prevents interruptions to pending subscriptions on initial load
    if (this.entityTypeListLoaded) {
      this.ngUnsubscribe.next();
    }

    this._dataService.getTreeEntities(selectedEntityType, this.formatDate(this.dateFrom), this.formatDate(this.dateTo))
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(entityList => {
        if (entityList.length > 0) {
          const treeData = this.formatTreeData(entityList);
          this.myTree = this.orderTree(treeData);
        } else {
          const emptyResponseEntity: TreeEntity[] = [{
            entityId: '0',
            entityName: 'No results. Try changing your filter parameters.',
            entityParentId: '0',
            entityBold: 0,
            entityDisabled: 1,
            entityType: 0,
            entityLevel: 1,
            entitySequence: 1
          }];

          this.myTree = this.orderTree(this.formatTreeData(emptyResponseEntity));
        }
      }, error => {
        error = error.error;
        if (error.Description) {
          alert(error.Description);
        } else {
          alert('Server not available, please contact support@inzenius.com');
        }
    });
  }

  formatTreeData(arr: TreeEntity[]): TreeEntity[] {
    const map = {};
    const roots: TreeEntity[] = [];
    let node: TreeEntity;

    // map nodes
    for (let i = 0; i < arr.length; i++) {
      node = arr[i];
      node.children = [];
      map[node.entityId] = i; // use map to look-up the parents
    }

    // push nodes
    for (let i = 0; i < arr.length; i++) {
      node = arr[i];

      if (node.entityParentId !== '0') {
        arr[map[node.entityParentId]].children.push(node);
      } else {
        roots.push(node);
      }
    }

    return roots;
  }

  orderTree(arr: TreeEntity[]): TreeEntity[] {
    if (arr.length > 1) {
      arr.sort((a, b) => a.entitySequence - b.entitySequence);
    }

    for (let i = 0; i < arr.length; i++) {
      this.orderTree(arr[i].children);
    }

    return arr;
  }

  expandTree(event: CheckboxEvent, arr: TreeEntity[]): void {
    for (const node of arr) {
      if (node.children.length) {
        this.expandTree(event, node.children);
      }
      node.visible = event.srcElement.checked;
    }
  }

  getSelectedEntities(arr: TreeEntity[]): string {
    for (const node of arr) {
      if (node.children.length) {
        this.getSelectedEntities(node.children);
      }
      if (node.selected) {
        this.selectedEntities += node.entityId + ',';
      }
    }

    return this.selectedEntities;
  }

  showRosters(entityType: string): void {
    // reset list first
    this.selectedEntities = '';
    const entitiesFilter = this.getSelectedEntities(this.myTree).slice(0, - 1);

    const eventData = {
      entitiesFilter: 'test',
      entityType: entityType,
      dateFrom: this.formatDate(this.dateFrom),
      dateTo: this.formatDate(this.dateTo)
    };

    this._dataService.filterData = eventData;
    this._dataService.showRosters.emit(eventData);
  }

  onDateValueChange(dateLabel: string, dateValue: Date, selectedEntityType: string): void {
    if (dateLabel === 'dateFrom' && this.formatDate(dateValue) !== this.formatDate(this.dateFrom)) {
      this.dateFrom = dateValue;

      localStorage.setItem('rosteringDateFrom', this.dateFrom.toISOString());
      this.getTreeEntities(selectedEntityType);

      // update filter data when user changes date
      // we need an accurate filter data for pasting rosters
      if (this._dataService.filterData) { this._dataService.filterData.dateFrom = this.formatDate(this.dateFrom); }
    }

    if (dateLabel === 'dateTo' && this.formatDate(dateValue) !== this.formatDate(this.dateTo)) {
      this.dateTo = dateValue;

      localStorage.setItem('rosteringDateTo', this.dateTo.toISOString());
      this.getTreeEntities(selectedEntityType);

      // update filter data when user changes date
      // we need an accurate filter data for pasting rosters
      if (this._dataService.filterData) { this._dataService.filterData.dateTo = this.formatDate(this.dateTo); }
    }
  }

  // calcuate the number of days between two dates
  getDateRangeLength(from: Date, to: Date): number {
    return moment(to).diff(from, 'days');
  }

  prevPeriod(): void {
    const range = this.getDateRangeLength(this.dateFrom, this.dateTo);

    // set dateTo for previous period to one day before current dateFrom
    const prevDateTo = new Date(this.dateFrom);
    prevDateTo.setDate(prevDateTo.getDate() - 1);

    // set dateFrom for previous period to `x` days before the dateTo we just calculated
    const prevDateFrom = new Date(prevDateTo);
    prevDateFrom.setDate(prevDateFrom.getDate() - range);

    this.adjustPeriod(prevDateFrom, prevDateTo);
  }

  nextPeriod(): void {
    const range = this.getDateRangeLength(this.dateFrom, this.dateTo);

    // set dateFrom for next period to one day after current dateTo
    const nextDateFrom = new Date(this.dateTo);
    nextDateFrom.setDate(nextDateFrom.getDate() + 1);

    // set dateTo for next period to `range` days after the dateFrom we just calculated
    const nextDateTo = new Date(nextDateFrom);
    nextDateTo.setDate(nextDateTo.getDate() + range);

    this.adjustPeriod(nextDateFrom, nextDateTo);
  }

  // set new date to and date from then reload grid (if already loaded)
  adjustPeriod(from: any, to: any): void {
    this.dateFrom = from;
    this.dateTo = to;

    // save new dates
    localStorage.setItem('rosteringDateFrom', this.dateFrom.toISOString());
    localStorage.setItem('rosteringDateTo', this.dateTo.toISOString());

    // NOTE: Currently using the period adjustment buttons does not refresh the entity list.
    //       This means that entities in the new dates will not be bolded/unbolded, however
    //       it allows for persistance of the entity selection.
    //       This seems like a better trade-off until proper persistance of the entity selection is implemented

    // reload roster grid
    if (this._dataService.filterData) {
      this._dataService.filterData.dateFrom = this.formatDate(from);
      this._dataService.filterData.dateTo = this.formatDate(to);
      this._dataService.showRosters.emit(this._dataService.filterData);
    }
  }
}
