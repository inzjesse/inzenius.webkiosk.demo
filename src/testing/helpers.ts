import { EventEmitter } from '@angular/core';
import { Response, ResponseOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/of';
import * as wjcCore from 'wijmo/wijmo';

import { TreeEntity } from '../app/interfaces/tree-entity';
import { DLICosting } from '../app/interfaces/dli-costing';
import { ExistingRoster } from '../app/interfaces/existing-roster';
import { PaginationProperties } from '../app/interfaces/pagination-properties';
import { ReportSettings } from '../app/interfaces/report-settings';
import { UtilisationReportResponse } from '../app/interfaces/utilisation';

export class DataServiceStub {
  showRosters = new EventEmitter<any>();
  showRosterTimes = new EventEmitter<any>();
  showSuppGrid = new EventEmitter<any>();
  showPreviousPeriod = new EventEmitter<any>();
  showNextPeriod = new EventEmitter<any>();
  logMessage = new EventEmitter<any>();
  changeDataOption = new EventEmitter<any>();
  showAddEngagementPopup = new EventEmitter<any>();
  loading = false;
  userReportTypes = [];
  gridData = new wjcCore.CollectionView();
  showFilterReportPopup = new EventEmitter<any>();
  showReportLoadedToViewer = new EventEmitter<any>();
  reportFilterChanged = new EventEmitter<any>();
  reportPageChanged = new EventEmitter<any>();
  reportSearchText = new EventEmitter<any>();
  reportPrint = new EventEmitter<any>();
  reportDownload = new EventEmitter<any>();
  reportExcelExport = new EventEmitter<any>();
  refreshViewer = new EventEmitter<any>();
  reportSettingsFilter: ReportSettings = {
    reportName: 'Test Report',
    rosterType: [{ planned: true, timekeeping: false, authorised: false }],
    reportFormat: [{ breakTime: false, hidEndTimes: false }],
    colOptions: [{ task: true, position: false }]
  };
  selectedReport = 'Test Report';
  changeColumnOption = new EventEmitter<any>();


  isLoggedIn() { }

  canAccessReports() { }

  getUserOptions() {
    return Observable.of({});
  }

  getReportTypes() {
    return Observable.of([{ reportType: 'test', reportName: 'Test Report' }]);
  }

  getEntityTypes() {
    return Observable.of([{ entityName: 'Test Entity' }]);
  }

  getTreeEntities(entityType: string, dateFrom: string, dateTo: string) {
    return Observable.of(new Array<TreeEntity>());
  }

  getRosters(entitiesFilter: string, entityType: string,
    dateFrom: string, dateTo: string, employeeId?: number) {
    return Observable.of(new Array<ExistingRoster>());
  }

  getRosterReports(rosterIdList: number[]) {
    return Observable.of({});
  }

  getReportData(dateFrom: string, dateTo: string, entityType: string, entityFilterID: number) {
    return Observable.of(new Array<ExistingRoster>());
  }

  getDLIReportData(payPeriodId: number) {
    return Observable.of(new Array<DLICosting>());
  }

  getSiteInactivityReportData(dateFrom: string, dateTo: string, entityType: string, entityIdList: number[]) {
    return Observable.of([]);
  }

  getUtilisationReportData(dateFrom: string, dateTo: string, entityType: string, entityIdList: number[]) {
    return Observable.of(new Array<UtilisationReportResponse>());
  }

  getShowDateTimeInputByReportType(reportType: string) {

  }

  getUserReportDefaultSettingsFilter(reportName: string): ReportSettings {
    return this.reportSettingsFilter;
  }
}

export class PaginationServiceStub {
  getPager(totalItems: number, currentPage: number, pageSize: number): PaginationProperties {
    return {
      totalItems: 1,
      currentPage: 1,
      pageSize: 1,
      totalPages: 1,
      startPage: 1,
      endPage: 1,
      startIndex: 1,
      endIndex: 1,
      pages: [1]
    };
  }
}

export class CommandsStub {
  copyClipboard = [];

  appendShiftCommand = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
  prependShiftCommand = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
  addNewEngagementCommand = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
  deleteRosterCommand = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
  copyCommand = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
  pasteCommand = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
  rollBackCommand = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
  rollForwardCommand = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
  showSupplementaryGrid = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
}

export class ReportcommandsStub {
  filterReportCommand = {
    executeCommand: () => {},
    canExecuteCommand: () => {}
  };
}

export class RouterStub {}

// Only implements params and part of snapshot.params
export class ActivatedRouteStub {
  // ActivatedRoute.params is Observable
  private subject = new BehaviorSubject(this.testParams);
  params = Observable.of({ ef: '1', et: '324', from: '123', to: '123' });
  paramMap = Observable.of({ get: () => 1 });

  // Test parameters
  private _testParams: {};
  get testParams() { return this._testParams; }
  set testParams(params: {}) {
    this._testParams = params;
    this.subject.next(params);
  }

  // ActivatedRoute.snapshot.params
  get snapshot() {
    return { params: this.testParams };
  }
}
